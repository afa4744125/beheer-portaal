import React, { useState, useEffect } from 'react';
import './index.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { jwtDecode } from 'jwt-decode';
import { Provider } from 'react-redux';
import store from './redux/store';

import Sidebar from './pages/UI/sidebar';
import LoginPage from './pages/login/loginPage';
import PlayerList from './pages/players/playerList';
import PlayerEdit from './pages/players/playerEdit';
import PlayerAdd from './pages/players/playerAdd';
import PlayerImport from './pages/players/playerImport';

import OrganisationList from './pages/organisation/organisationList';
import OrganisationAdd from './pages/organisation/organisationAdd';
import OrganisationEdit from './pages/organisation/organisationEdit';
import ThemaEdit from './pages/organisation/themaEdit';
import OrganisationSettings from './pages/organisation/organisationSettings';

import DataTypeList from './pages/datatype/dataTypeList';

import Userlist from './pages/user/userList';
import UserAdd from './pages/user/userAdd';
import UserEdit from './pages/user/userEdit';

import TeamList from './pages/team/teamList';

import Dashboard from './pages/dashboard';
import Hotbar from './pages/UI/hotbar';

interface JwtPayload {
  exp: number;
  roles: string[];
}

function App() {
  const [token, setToken] = useState<string | null>(sessionStorage.getItem('token'));
  const [roles, setRoles] = useState<string[]>([]);


  const isTokenExpired = (token: string | null) => {
    if (!token) return true;

    try {
      const decoded = jwtDecode<JwtPayload>(token);
      return decoded.exp * 1000 < Date.now();
    } catch (error) {
      return true;
    }
  };

  useEffect(() => {
    const token = sessionStorage.getItem('token');
    if (isTokenExpired(token)) {
      sessionStorage.clear();
    }

    document.body.style.overflow = 'hidden';
    return () => {
      document.body.style.overflow = 'auto';
    };
  }, []);

  useEffect(() => {
    if (token) {
      try {
        const decoded = jwtDecode<JwtPayload>(token);
        setRoles(decoded.roles);
      } catch (error) {
        setRoles([]);
      }
    }
  }, [token]);

  useEffect(() => {
    const handleStorageChange = () => {
      setToken(sessionStorage.getItem('token'));
    };
    window.addEventListener('storage', handleStorageChange);

    return () => {
      window.removeEventListener('storage', handleStorageChange);
    };
  }, []);

  useEffect(() => {
    document.body.style.overflow = 'hidden';
    return () => {
      document.body.style.overflow = 'auto';
    };
  }, []);

  return (
    <Provider store={store}>
      <Router>
        {roles.includes('ROLE_USER') ? (
          <div style={{ display: 'flex', height: '100vh' }}>
            <Sidebar />
            <div style={{ flex: 1, display: 'flex', flexDirection: 'column', overflowY: 'auto' }}>
              <Hotbar />
              <div style={{ flex: 1 }}>
                <Routes>

                  <Route path="/" element={<Dashboard />} />

                  <Route path='player'>
                    <Route path="view" element={<PlayerList />} />
                  </Route>

                  {(roles.includes('ROLE_ADMIN') || roles.includes('ROLE_SUPER_ADMIN')) && (
                    <>
                      <Route path='player'>
                        <Route path="edit/:id" element={<PlayerEdit />} />
                        <Route path="add" element={<PlayerAdd />} />
                        <Route path="import" element={<PlayerImport />} />
                      </Route>

                      <Route path='club'>
                        <Route path="thema" element={<ThemaEdit />} />
                        <Route path="settings" element={<OrganisationSettings />} />
                        <Route path="user" element={<Userlist />} />
                        <Route path="add/user" element={<UserAdd />} />
                        <Route path="edit/user/:id" element={<UserEdit />} />
                      </Route>

                    </>
                  )}
                  <Route path='team' element={<TeamList />}>
                    <Route path="view" element={<TeamList />} />
                  </Route>

                  {roles.includes('ROLE_SUPER_ADMIN') && (
                    <Route path='admin'>
                      <Route path="club" element={<OrganisationList />} />
                      <Route path="add/club" element={<OrganisationAdd />} />
                      <Route path="edit/club/:id" element={<OrganisationEdit />} />
                      <Route path="datatype" element={<DataTypeList />} />
                    </Route>
                  )}
                </Routes>
              </div>
            </div>
          </div>
        ) : (
          <LoginPage />
        )}
      </Router>
    </Provider>
  );
}

export default App;
