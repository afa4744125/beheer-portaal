import React, { useState, useEffect } from 'react';
import Profile from '../../shared/components/image/profile';
import Dropdown from '../../shared/components/form/dropdown';
import { usePublicListApiQuery, useGetOrganisationApiQuery } from '../../redux/services/organisation';
import { jwtDecode } from 'jwt-decode';
import type { Organisations } from '../../redux/types/organisation';

interface JwtPayload {
    email: string;
    roles: string[];
    firstname: string;
    lastname: string;
}

interface Option {
    id: string;
    name: string;
}

const Card = () => {
    const [token] = useState<string | null>(sessionStorage.getItem('token'));
    const [roles, setRoles] = useState<string[]>([]);
    const [firstname, setFirstname] = useState<string>();
    const [lastname, setLastname] = useState<string>();

    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));

    const { data: organisation } = useGetOrganisationApiQuery(org_id!);
    const { data } = usePublicListApiQuery();

    useEffect(() => {
        if (token) {
            try {
                const decoded = jwtDecode<JwtPayload>(token);
                setRoles(decoded.roles);
                setFirstname(decoded.firstname);
                setLastname(decoded.lastname);
            } catch (error) {
                setRoles(['']);
            }
        }
    }, [token]);

    const dropdownOptions: Option[] = data
        ? data.map((item: Organisations) => ({ id: item.id, name: item.name }))
        : [];


    const defaultValue = dropdownOptions.find(option => option.id === org_id) || null;

    return (
        <>
            <div style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: '#212232',
                marginLeft: '30px',
                borderBottomLeftRadius: '30px',
                padding: '20px 30px',
                position: 'relative',
            }}>
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <h1 style={{ margin: 0, fontSize: '25px', color: '#fff' }}>Welkom:</h1>
                    <h1 style={{ margin: 0, fontSize: '25px', paddingLeft: '15px', color: '#fff' }}>
                        {firstname} {lastname}
                    </h1>
                </div>
                {roles.includes('ROLE_SUPER_ADMIN') ? (
                    <div style={{
                        position: 'absolute',
                        left: '50%',
                        bottom: '0',
                        transform: 'translate(-50%, -50%)',
                        zIndex: '999'
                    }}>
                        <Dropdown
                            value={defaultValue?.id}
                            options={dropdownOptions}
                            onChange={(selectedValue) => {
                                sessionStorage.setItem('org_id', selectedValue);
                                window.location.href = '/';
                            }}
                        />
                    </div>
                ) : (
                    <h1 style={{
                        fontSize: '20px',
                        color: '#aaa',
                        position: 'absolute',
                        left: '50%',
                        bottom: '0',
                        transform: 'translate(-50%, -50%)',
                    }}>
                        {organisation?.name}
                    </h1>
                )}
                <Profile />
            </div>
        </>
    );
}

export default Card;
