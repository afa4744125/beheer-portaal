import React, { useState, useEffect } from 'react';
import Banner from '../../shared/components/image/banner'
import SidebarItem from '../../shared/components/sidebar/sidebarItem';
import { SidebarProvider } from '../../shared/components/sidebar/sidebarContext';
import AfaLogo from '../../assets/AfaLogobg.png';
import { faUserPlus, faUsers, faUser, faChartLine, faBuilding, faPalette, faGear, faUserGroup, faUsersViewfinder, faIdCardClip, faLock, faSitemap, faDatabase } from '@fortawesome/free-solid-svg-icons';
import { jwtDecode } from 'jwt-decode';
import { useGetThemaApiQuery } from '../../redux/services/organisation';

interface JwtPayload {
    roles: string[];
}

const Sidebar = () => {
    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));
    const [token] = useState<string | null>(sessionStorage.getItem('token'));

    const [roles, setRoles] = useState<string[]>([]);

    const { data: thema } = useGetThemaApiQuery(org_id!);

    const primaryColor = thema?.primaryColor ?? '#ffffff';
    const secondaryColor = thema?.secondaryColor ?? '#ffffff';
    const logo = thema?.logo ?? AfaLogo;

    useEffect(() => {
        if (token) {
            try {
                const decoded = jwtDecode<JwtPayload>(token);
                setRoles(decoded.roles);
            } catch (error) {
                console.error('Invalid token:', error);
                setRoles([]);
            }
        }
    }, [token]);

    return (
        <>

            <div style={{ height: 'auto', width: '300px', backgroundColor: '#212232' }}>
                <Banner primaryColor={primaryColor} secondaryColor={secondaryColor} logo={logo} style={{ marginBottom: '48px' }} />
                <SidebarProvider>
                    <SidebarItem label="Dashboard" route="/" icon={faChartLine} />
                    <SidebarItem label="Speler" icon={faUser}>
                        <SidebarItem label="Overzicht" route="/player/view" icon={faUsers} />
                        {(roles.includes('ROLE_ADMIN') || roles.includes('ROLE_SUPER_ADMIN')) && (
                            <SidebarItem label="Aanmaken" route="/player/add" icon={faUserPlus} />
                        )}
                    </SidebarItem>
                    {(roles.includes('ROLE_ADMIN') || roles.includes('ROLE_SUPER_ADMIN')) && (
                        <SidebarItem label="Club" icon={faBuilding}>
                            <SidebarItem label="Thema" route="/club/thema" icon={faPalette} />
                            <SidebarItem label="Instelling" route="/club/settings" icon={faGear} />
                            <SidebarItem label="Gebruiker" route="/club/user" icon={faIdCardClip} />
                        </SidebarItem>
                    )}
                    <SidebarItem label="Team" icon={faUserGroup}>
                        <SidebarItem label="Overzicht" route="/team/view" icon={faUsersViewfinder} />
                    </SidebarItem>
                    {roles.includes('ROLE_SUPER_ADMIN') && (
                        <SidebarItem label="Admin" icon={faLock}>
                            <SidebarItem label="clubs" route="/admin/club" icon={faSitemap} />
                            <SidebarItem label="Data types" route="/admin/datatype" icon={faDatabase} />
                        </SidebarItem>
                    )}
                </SidebarProvider>
            </div>
        </>
    );
}

export default Sidebar;
