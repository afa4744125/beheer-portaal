import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash, faTimes, faCheck } from '@fortawesome/free-solid-svg-icons';
import { useAdminListApiQuery, useDeleteDataTypeApiMutation, useCreateDataTypeApiMutation, useUpdateDataTypeApiMutation } from '../../redux/services/datatype';
import { DataTypes, Status, DataType } from '../../redux/types/datatype';
import InputField from '../../shared/components/form/inputfield';
import Switch from '../../shared/components/form/switch';
import Button from '../../shared/components/form/button';
import Succes from '../../shared/components/form/succes';
import Error from '../../shared/components/form/error';

const DataTypeList = () => {

    const status = [
        { id: 'active', name: 'actief' },
        { id: 'inactive', name: 'inactief' }
    ];

    const [filterText, setFilterText] = useState('');

    const { data, refetch } = useAdminListApiQuery();
    const [deleteDataType] = useDeleteDataTypeApiMutation();
    const [createDataType] = useCreateDataTypeApiMutation();
    const [updateDataType] = useUpdateDataTypeApiMutation();

    const [showModal, setShowModal] = useState(false);
    const [showEditModal, setShowEditModal] = useState(false);


    const [DataTypeName, setDataTypeName] = useState('');
    const [DataTypeStatus, setDataTypeStatus] = useState(status[1]);
    const [DataTypeNumber, setDataTypeNumber] = useState(false);

    const [selectedDataTypeId, setSelectedDataTypeId] = useState<string | null>(null);

    const [error, setError] = useState('');
    const [succes, setSucces] = useState('');

    const filteredDataType: DataTypes[] =
        Array.isArray(data) ? data.filter((DataType: DataTypes) =>
            DataType.name.toLowerCase().includes(filterText.toLowerCase())
        ) : [];

    const handleEdit = (id: string, currentName: string, currentStatus: string, currentNumber: boolean) => {
        setSelectedDataTypeId(id);
        setDataTypeName(currentName);
        setDataTypeStatus(currentStatus === status[0].id ? status[0] : status[1]);
        setDataTypeNumber(currentNumber);
        setShowEditModal(true);
    };

    const handleDelete = async (id: string) => {
        const confirmDelete = window.confirm('Weet je het zeker dat je data type wil delete?');
        if (confirmDelete) {
            try {
                await deleteDataType(id).unwrap();
                refetch();
                setSucces('het delete van data type is gelukt');
            } catch (error) {
                setError('Helaas is het niet gelukt om de data tyoe te delete');
            }
        }
    };

    const getStatusColor = (status: Status) => {
        return status === 'active' ? '#28a745' : '#dc3545';
    };

    const handleAdd = () => {
        setShowModal(true);
    };

    const handleModalClose = () => {
        setShowModal(false);
        setShowEditModal(false);
        setDataTypeNumber(false);
        setDataTypeName('');
        setDataTypeStatus(status[1]);
    };

    const handleModalSubmit = async () => {
        if (DataTypeName) {

            const DataType: DataType = {
                name: DataTypeName,
                status: DataTypeStatus.id as Status,
                number: DataTypeNumber
            }

            await createDataType(DataType).unwrap();
            setSucces('Data type succesvol toegevoegd');

            setShowModal(false);
            setDataTypeNumber(false);
            setDataTypeName('');
            setDataTypeStatus(status[1]);

            refetch();
        } else {
            setError('Data type naam is verplicht');
        }
    };

    const handleEditSubmit = async () => {
        if (selectedDataTypeId && DataTypeName) {

            const DataType: DataTypes = {
                id: selectedDataTypeId,
                name: DataTypeName,
                status: DataTypeStatus.id as Status,
                number: DataTypeNumber
            }

            await updateDataType(DataType).unwrap();
            setSucces('Data type succesvol bijgewerkt');

            setShowEditModal(false);
            setDataTypeNumber(false);
            setDataTypeName('');
            setDataTypeStatus(status[1]);

            refetch();
        } else {
            setError('Data type naam is verplicht');
        }
    };

    const SuccesReset = () => {
        setSucces('');
    };

    const ErrorReset = () => {
        setError('');
    };

    return (
        <>

            {showModal && (
                <div style={{
                    position: 'fixed',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    zIndex: 1000,
                }}>
                    <div style={{
                        backgroundColor: '#212232',
                        padding: '20px',
                        borderRadius: '10px',
                        position: 'relative',
                        width: '400px',
                        maxWidth: '100%',
                        textAlign: 'center',
                        border: '1px solid white',
                    }}>
                        <button
                            onClick={handleModalClose}
                            style={{
                                position: 'absolute',
                                top: '5px',
                                right: '10px',
                                background: 'none',
                                border: 'none',
                                color: 'white',
                                fontSize: '20px',
                                cursor: 'pointer',
                            }}
                        >
                            <FontAwesomeIcon icon={faTimes} />
                        </button>
                        <h2 style={{ color: 'white', marginBottom: '20px' }}>Voeg een nieuw DataType toe</h2>

                        <InputField
                            placeholder='DataType naam'
                            value={DataTypeName}
                            onChange={(e) => setDataTypeName(e.target.value)}
                            style={{ width: '400px', marginBottom: '20px' }}
                        />

                        <Switch checked={DataTypeStatus.id === status[0].id} onChange={(checked) => setDataTypeStatus(checked ? status[0] : status[1])} title='Status' style={{ marginBottom: '20px' }} />

                        <Switch checked={DataTypeNumber} onChange={(checked) => setDataTypeNumber(checked)} title='Is nummer' style={{ marginBottom: '20px' }} />

                        <button
                            onClick={handleModalSubmit}
                            style={{
                                backgroundColor: '#4CAF50',
                                border: 'none',
                                padding: '10px 20px',
                                borderRadius: '5px',
                                color: 'white',
                                fontSize: '16px',
                                cursor: 'pointer',
                                marginRight: '10px',
                            }}
                        >
                            <FontAwesomeIcon icon={faCheck} /> Bevestigen
                        </button>
                    </div>
                </div>
            )}

            {showEditModal && (
                <div style={{
                    position: 'fixed',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    zIndex: 1000,
                }}>
                    <div style={{
                        backgroundColor: '#212232',
                        padding: '20px',
                        borderRadius: '10px',
                        position: 'relative',
                        width: '400px',
                        maxWidth: '100%',
                        textAlign: 'center',
                        border: '1px solid white',
                    }}>
                        <button
                            onClick={handleModalClose}
                            style={{
                                position: 'absolute',
                                top: '5px',
                                right: '10px',
                                background: 'none',
                                border: 'none',
                                color: 'white',
                                fontSize: '20px',
                                cursor: 'pointer',
                            }}
                        >
                            <FontAwesomeIcon icon={faTimes} />
                        </button>
                        <h2 style={{ color: 'white', marginBottom: '20px' }}>Bewerk DataType</h2>

                        <InputField
                            placeholder='DataType naam'
                            value={DataTypeName}
                            onChange={(e) => setDataTypeName(e.target.value)}
                            style={{ width: '400px', marginBottom: '20px' }}
                        />

                        <Switch checked={DataTypeStatus.id === status[0].id} onChange={(checked) => setDataTypeStatus(checked ? status[0] : status[1])} title='Status' style={{ marginBottom: '20px' }} />

                        <Switch checked={DataTypeNumber} onChange={(checked) => setDataTypeNumber(checked)} title='Is nummer' style={{ marginBottom: '20px' }} />

                        <button
                            onClick={handleEditSubmit}
                            style={{
                                backgroundColor: '#4CAF50',
                                border: 'none',
                                padding: '10px 20px',
                                borderRadius: '5px',
                                color: 'white',
                                fontSize: '16px',
                                cursor: 'pointer',
                                marginRight: '10px',
                            }}
                        >
                            <FontAwesomeIcon icon={faCheck} /> Bevestigen
                        </button>
                    </div>
                </div>
            )}

            <div style={{
                padding: '20px',
                backgroundColor: '#212232',
                borderRadius: '20px',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                margin: '25px',
                paddingBottom: '50px',
            }}>
                <div style={{
                    marginTop: '15px',
                    display: 'flex',
                    justifyContent: 'space-between',
                    width: '100%',
                    maxWidth: '100%',
                }}>
                    <input
                        type="search"
                        placeholder="zoek dataType..."
                        value={filterText}
                        onChange={(e) => setFilterText(e.target.value)}
                        className='focus:outline-none'
                        style={{
                            width: '350px',
                            padding: '10px 18px',
                            background: '#2a2a3d',
                            border: '1px solid #444',
                            fontSize: '16px',
                            boxSizing: 'border-box',
                            marginRight: '10px',
                            transition: 'border-color 0.3s',
                            borderRadius: '5px',
                        }}
                    />
                    <div>
                        <Button
                            onclick={handleAdd}
                            name='Toevoegen'
                            style={{ width: '150px', marginLeft: '30px' }}
                        />
                    </div>
                </div>

                <Error message={error} onClose={ErrorReset} style={{ width: '100%' }} />
                <Succes message={succes} onClose={SuccesReset} style={{ width: '100%' }} />

                <table style={{
                    width: '100%',
                    borderCollapse: 'collapse',
                    borderRadius: '10px',
                    boxShadow: '0 2px 10px rgba(0, 0, 0, 0.05)',
                    marginTop: '20px',
                }}>
                    <thead>
                        <tr style={{
                            color: 'white',
                        }}>
                            <th style={{
                                fontSize: '22px',
                                fontWeight: 'bold',
                                padding: '15px',
                                borderBottom: '2px solid white',
                                textAlign: 'center',
                            }} colSpan={2}>
                                DataTypes
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {filteredDataType.length > 0 ? (
                            filteredDataType.map((DataType: DataTypes) => (
                                <tr key={DataType.id} style={{
                                    borderBottom: '1px solid white',
                                    transition: 'background-color 0.3s',
                                }}
                                    onMouseEnter={(e) => { e.currentTarget.style.backgroundColor = '#1e202d'; }}
                                    onMouseLeave={(e) => { e.currentTarget.style.backgroundColor = 'transparent'; }}
                                >
                                    <td style={{ padding: '15px', color: 'white', display: 'flex', alignItems: 'center', fontSize: '20px', }}>
                                        <span style={{
                                            width: '12px',
                                            height: '12px',
                                            borderRadius: '50%',
                                            backgroundColor: getStatusColor(DataType.status),
                                            marginRight: '15px',
                                        }}></span>
                                        {DataType.name}
                                    </td>
                                    <td style={{
                                        padding: '15px 20px',
                                        textAlign: 'right',
                                    }}>
                                        <button onClick={() => handleEdit(DataType.id, DataType.name, DataType.status, DataType.number)} title="Edit" style={{
                                            marginRight: '20px',
                                            backgroundColor: 'transparent',
                                            border: 'none',
                                            cursor: 'pointer',
                                        }}>
                                            <FontAwesomeIcon icon={faEdit} style={{ fontSize: '20px', color: 'lightblue' }} />
                                        </button>
                                        <button onClick={() => handleDelete(DataType.id)} title="Delete" style={{
                                            backgroundColor: 'transparent',
                                            border: 'none',
                                            cursor: 'pointer',
                                        }}>
                                            <FontAwesomeIcon icon={faTrash} style={{ fontSize: '22px', color: 'red' }} />
                                        </button>
                                    </td>
                                </tr>
                            ))
                        ) : (
                            <tr>
                                <td colSpan={2} style={{
                                    color: 'white',
                                    textAlign: 'center',
                                    padding: '15px',
                                }}>
                                    Geen DataTypes gevonden
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>

            <style>
                {`
                    input[type="search"]::-webkit-search-cancel-button {
                        -webkit-filter: invert(50%); /* Invert the color to make it white if background is dark */
                    }
                `}
            </style>
        </>
    );
};

export default DataTypeList;
