import React, { useState } from 'react';
import Button from '../../shared/components/form/button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { useUsersApiQuery, useDeleteUserApiMutation } from '../../redux/services/user';
import type { User } from '../../redux/types/user';
import { useNavigate } from 'react-router-dom';

const UserList = () => {
    const [filterText, setFilterText] = useState('');
    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));

    const { data, refetch } = useUsersApiQuery(org_id || '', { skip: !org_id });

    const [deleteUser] = useDeleteUserApiMutation();

    const navigate = useNavigate();

    const filteredUser: User[] =
        Array.isArray(data) ? data.filter((org: User) =>
            org.firstname.toLowerCase().includes(filterText.toLowerCase()) ||
            org.lastname.toLowerCase().includes(filterText.toLowerCase()) ||
            `${org.firstname.toLowerCase()} ${org.lastname.toLowerCase()}`.includes(filterText.toLowerCase())
        ) : [];


    const handleEdit = (id: string) => {
        navigate(`/club/edit/user/${id}`);
    };

    const handleDelete = async (id: string) => {
        const confirmDelete = window.confirm('Are you sure you want to delete this user?');
        if (confirmDelete) {
            await deleteUser(id).unwrap();
            refetch();
        }
    };

    const handleAdd = () => {
        navigate('/club/add/user');
    };

    return (
        <div style={{
            padding: '20px',
            backgroundColor: '#212232',
            borderRadius: '20px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            margin: '25px',
            paddingBottom: '50px',
        }}>
            <div style={{
                marginTop: '15px',
                display: 'flex',
                justifyContent: 'space-between',
                width: '100%',
                maxWidth: '100%',
            }}>
                <input
                    type="text"
                    placeholder="zoek user..."
                    value={filterText}
                    onChange={(e) => setFilterText(e.target.value)}
                    className='focus:outline-none'
                    style={{
                        width: '350px',
                        padding: '10px 18px',
                        background: '#2a2a3d',
                        border: '1px solid #444',
                        fontSize: '16px',
                        boxSizing: 'border-box',
                        marginRight: '10px',
                        transition: 'border-color 0.3s',
                        borderRadius: '5px',
                    }}
                />
                <Button
                    name='Toevoegen'
                    style={{ width: '150px' }}
                    onclick={handleAdd}
                />
            </div>

            <table style={{
                width: '100%',
                borderCollapse: 'collapse',
                borderRadius: '10px',
                boxShadow: '0 2px 10px rgba(0, 0, 0, 0.05)',
                marginTop: '20px',
            }}>
                <thead>
                    <tr style={{
                        color: 'white',
                    }}>
                        <th style={{
                            fontSize: '22px',
                            fontWeight: 'bold',
                            padding: '15px',
                            borderBottom: '2px solid white',
                            textAlign: 'center',
                        }} colSpan={3}>
                            User
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {filteredUser.length > 0 ? (
                        filteredUser.map((user: User) => (
                            <tr key={user.id} style={{
                                borderBottom: '1px solid white',
                                transition: 'background-color 0.3s',
                            }}
                                onMouseEnter={(e) => { e.currentTarget.style.backgroundColor = '#1e202d'; }}
                                onMouseLeave={(e) => { e.currentTarget.style.backgroundColor = 'transparent'; }}
                            >
                                <td style={{
                                    fontSize: '20px',
                                    color: 'white',
                                    padding: '15px 20px',
                                }}>
                                    {user.firstname + ' ' + user.lastname}
                                </td>
                                <td style={{
                                    fontSize: '20px',
                                    color: 'white',
                                    padding: '15px 20px',
                                }}>
                                    {user.role.includes('ROLE_ADMIN') ? "beheerder" : 'gebruiker'}
                                </td>
                                <td style={{
                                    padding: '15px 20px',
                                    textAlign: 'right',
                                }}>
                                    <button onClick={() => handleEdit(user.id)} title="Edit" style={{
                                        marginRight: '20px',
                                        backgroundColor: 'transparent',
                                        border: 'none',
                                        cursor: 'pointer',
                                    }}>
                                        <FontAwesomeIcon icon={faEdit} style={{ fontSize: '20px', color: 'lightblue' }} />
                                    </button>
                                    <button onClick={() => handleDelete(user.id)} title="Delete" style={{
                                        backgroundColor: 'transparent',
                                        border: 'none',
                                        cursor: 'pointer',
                                    }}>
                                        <FontAwesomeIcon icon={faTrash} style={{ fontSize: '22px', color: 'red' }} />
                                    </button>
                                </td>
                            </tr>
                        ))
                    ) : (
                        <tr>
                            <td colSpan={3} style={{
                                color: 'white',
                                textAlign: 'center',
                                padding: '15px',
                            }}>
                                Geen user gevonden
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    );
};

export default UserList;
