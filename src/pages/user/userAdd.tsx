import React, { useState } from 'react';
import validator from 'validator';
import InputField from '../../shared/components/form/inputfield';
import PasswordInputfield from '../../shared/components/form/passwordInputfield';
import Button from '../../shared/components/form/button';
import Dropdown from '../../shared/components/form/dropdown';
import { useCreateUserApiMutation } from '../../redux/services/user';
import { useTeamsApiQuery } from '../../redux/services/team';
import { useNavigate } from 'react-router-dom';
import { CreateUser, Role } from '../../redux/types/user';
import Succes from '../../shared/components/form/succes';
import Error from '../../shared/components/form/error';
import type { Team } from '../../redux/types/team';

interface Option {
    id: string;
    name: string;
}

const UserAdd = () => {

    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));

    const roles = [
        { id: 'ROLE_USER', name: 'Gebruiker' },
        { id: 'ROLE_ADMIN', name: 'Beheerder' }
    ];

    const { data: teams } = useTeamsApiQuery(org_id || '', { skip: !org_id });

    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [email, setEmail] = useState('');
    const [role, setRole] = useState(roles[0]);
    const [team, setTeam] = useState<Team[]>([]);
    const [password, setPassword] = useState('');
    const [conPassword, setconPassword] = useState('');

    const navigate = useNavigate();

    const [error, setError] = useState('');
    const [succes, setSucces] = useState('');

    const [createUser, { isLoading }] = useCreateUserApiMutation();

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            if (org_id) {

                if (!validator.isEmail(email)) {
                    setError('Ongeldig e-mailadres');
                    return;
                }

                if (!validator.isStrongPassword(password)) {
                    setError('Wachtwoord is niet sterk genoeg, moet minimaal 1 hoofdletetr, 1 special teken, 1 nummer, 8 letters hebben')
                    return;
                }

                if (password !== conPassword) {
                    setError('Wachtwoorden komen niet overeen')
                    return;
                }

                const user: CreateUser = {
                    firstname,
                    lastname,
                    email,
                    password,
                    org_id,
                    role: [role.id as Role],
                    teams: team.map((item) => item.id)
                };

                await createUser(user).unwrap();

                setError('');
                setSucces('Succesvol create gebruiker');

                setTimeout(() => {
                    navigate('/club/user');
                }, 2000);
            }
        } catch (err) {
            setError('Er is iets mis gegaan met toevoegen van de gebruiker')
        }
    };

    const teamOptions: Option[] = teams
        ? teams
            .filter((item: Team) => !team.some((selectedTeam) => selectedTeam.id === item.id))
            .map((item: Team) => ({ id: item.id, name: item.name }))
        : [];

    const handleRemoveTeam = (id: string) => {
        setTeam((prevTeams) => prevTeams.filter((team) => team.id !== id));
    };

    const SuccesReset = () => {
        setSucces('');
    };

    const ErrorReset = () => {
        setError('');
    };

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                margin: '30px',
                height: '100%',
            }}
        >
            <div
                style={{
                    padding: '30px',
                    backgroundColor: '#212232',
                    borderRadius: '15px',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    margin: '0 auto',
                    width: '100%',
                    maxWidth: '650px',
                    boxShadow: '0 4px 12px rgba(0, 0, 0, 0.3)',
                    boxSizing: 'border-box',
                    transition: 'all 0.3s ease',
                }}>
                <h1 style={{ fontSize: '34px', marginBottom: '40px' }}>User Aanmaken</h1>
                <Error message={error} onClose={ErrorReset} style={{ width: '100%' }} />
                <Succes message={succes} onClose={SuccesReset} style={{ width: '100%' }} />
                <form onSubmit={handleSubmit} style={{ width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    <InputField
                        label='Voornaam'
                        placeholder='Voornaam'
                        value={firstname}
                        onChange={(e) => setFirstname(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                        required
                    />

                    <InputField
                        label='Achternaam'
                        placeholder='Achternaam'
                        value={lastname}
                        onChange={(e) => setLastname(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                        required
                    />

                    <InputField
                        label='Email'
                        placeholder='Email'
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                        required
                    />

                    <PasswordInputfield
                        label='Wachtwoord'
                        placeholder='wachtwoord'
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                        required
                    />

                    <PasswordInputfield
                        label='Bevestig wachtwoord'
                        placeholder='wachtwoord'
                        value={conPassword}
                        onChange={(e) => setconPassword(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                        required
                    />

                    <Dropdown
                        style={{ width: '600px', marginBottom: '30px' }}
                        label='Role'
                        value={role?.id}
                        options={roles}
                        noDefaultSearch
                        onChange={(selectedId) => {
                            const selectedRole = roles.find((r) => r.id === selectedId);
                            if (selectedRole) {
                                setRole(selectedRole);
                            }
                        }}
                    />

                    {role.id === 'ROLE_USER' && (
                        <>
                            <Dropdown
                                style={{ width: '600px', marginBottom: '30px' }}
                                label='Teams'
                                options={teamOptions}
                                noDefaultSearch
                                onChange={(selectedId) => {
                                    if (teams) {
                                        const selectedTeam = teams.find((r) => r.id === selectedId);
                                        if (selectedTeam) {
                                            setTeam((prevTeams) => {
                                                const isAlreadySelected = prevTeams.some((t) => t.id === selectedId);
                                                return isAlreadySelected ? prevTeams : [...prevTeams, selectedTeam];
                                            });
                                        }
                                    }
                                }}
                            />
                            {team.length > 0 && (
                                <div style={{ position: 'relative', marginBottom: '30px', width: '600px', textAlign: 'center' }}>
                                    <label style={{
                                        marginBottom: '10px',
                                        color: '#ffffff',
                                        fontWeight: '600',
                                        fontSize: '14px',
                                        margin: 'auto',
                                        display: 'inline-block'
                                    }}>
                                        Alle geselecteerd teams
                                    </label>
                                    <div style={{ display: 'flex', flexWrap: 'wrap', minWidth: '100%', justifyContent: 'center' }}>
                                        {team.map((t) => (
                                            <div
                                                key={t.id}
                                                style={{
                                                    display: 'flex',
                                                    alignItems: 'center',
                                                    padding: '8px 18px',
                                                    border: '1px solid #444',
                                                    backgroundColor: '#2a2a3d',
                                                    borderRadius: '20px',
                                                    margin: '5px',
                                                    color: '#fff',
                                                    transition: 'border-color 0.3s ease, background-color 0.3s ease, box-shadow 0.3s ease',
                                                    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.5)',
                                                }}
                                            >
                                                {t.name}
                                                <button
                                                    onClick={() => handleRemoveTeam(t.id)}
                                                    style={{
                                                        marginLeft: '10px',
                                                        cursor: 'pointer',
                                                        background: 'none',
                                                        border: 'none',
                                                        color: '#fff',
                                                    }}
                                                >
                                                    ×
                                                </button>
                                            </div>
                                        ))}
                                    </div>
                                </div>

                            )}
                        </>
                    )}

                    <Button
                        name='Toevoegen'
                        isLoading={isLoading}
                        style={{ width: '150px' }}
                    />

                </form>
            </div >
        </div >
    );
};

export default UserAdd;