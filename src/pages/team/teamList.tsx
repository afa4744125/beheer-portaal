import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faEye, faTrash, faTimes, faCheck } from '@fortawesome/free-solid-svg-icons';
import { useTeamsApiQuery, useDeleteTeamApiMutation, useCreateTeamApiMutation, useUpdateTeamApiMutation } from '../../redux/services/team';
import { useGetOrganisationApiQuery } from '../../redux/services/organisation';
import type { Team } from '../../redux/types/team';
import InputField from '../../shared/components/form/inputfield';
import Button from '../../shared/components/form/button';
import { useNavigate } from 'react-router-dom';
import Succes from '../../shared/components/form/succes';
import Error from '../../shared/components/form/error';

const TeamList = () => {
    const [filterText, setFilterText] = useState('');
    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));

    const { data, refetch } = useTeamsApiQuery(org_id || '', { skip: !org_id });
    const { data: organisation } = useGetOrganisationApiQuery(org_id!);
    const [deleteTeam] = useDeleteTeamApiMutation();
    const [createTeam] = useCreateTeamApiMutation();
    const [updateTeam] = useUpdateTeamApiMutation();

    const [showModal, setShowModal] = useState(false);
    const [showEditModal, setShowEditModal] = useState(false);
    const [teamName, setTeamName] = useState('');
    const [selectedTeamId, setSelectedTeamId] = useState<string | null>(null);

    const [isExporting, setIsExporting] = useState(false);

    const [error, setError] = useState('');
    const [succes, setSucces] = useState('');

    const navigate = useNavigate();

    const filteredTeam: Team[] =
        Array.isArray(data) ? data.filter((team: Team) =>
            team.name.toLowerCase().includes(filterText.toLowerCase())
        ) : [];

    const handleEdit = (id: string, currentName: string) => {
        setSelectedTeamId(id);
        setTeamName(currentName);
        setShowEditModal(true);
    };

    const handleDelete = async (id: string) => {
        const confirmDelete = window.confirm('Are you sure you want to delete this team?');
        if (confirmDelete) {
            await deleteTeam(id).unwrap();
            refetch();
        }
    };

    const handleAdd = () => {
        setShowModal(true);
    };

    const handleModalClose = () => {
        setShowModal(false);
        setShowEditModal(false);
        setTeamName('');
    };

    const handleModalSubmit = async () => {
        if (teamName && org_id) {
            await createTeam({ name: teamName, org_id }).unwrap();
            setSucces('Team succesvol toegevoegd');
            setShowModal(false);
            setTeamName('');
            refetch();
        } else {
            setError('Team naam is verplicht');
        }
    };

    const handleEditSubmit = async () => {
        if (selectedTeamId && teamName) {
            await updateTeam({ id: selectedTeamId, name: teamName }).unwrap();
            setSucces('Team succesvol bijgewerkt');
            setShowEditModal(false);
            setTeamName('');
            refetch();
        } else {
            setError('Team naam is verplicht');
        }
    };

    const handleView = (id: string) => {
        navigate('/player/view?team_id=' + id);
    };

    const generateCSV = (data: Team[]) => {
        const csvRows: string[] = [];

        const headers = ['ID', 'Name'];
        csvRows.push(headers.join(';'));

        for (const team of data) {
            const values = [team.id, team.name];
            csvRows.push(values.join(';'));
        }

        return csvRows.join('\n');
    };

    const handleExport = () => {
        setIsExporting(true)
        if (!filteredTeam.length) {
            setError('Er zijn geen teams om te exporteren');
            return;
        }

        const csvData = generateCSV(filteredTeam);
        const blob = new Blob([csvData], { type: 'text/csv' });
        const url = URL.createObjectURL(blob);

        const now = new Date();
        const dateString = now.toISOString().slice(0, 10);

        const hours = String(now.getHours()).padStart(2, '0');
        const minutes = String(now.getMinutes()).padStart(2, '0');
        const seconds = String(now.getSeconds()).padStart(2, '0');

        const timeString = `${hours}${minutes}${seconds}`;

        const a = document.createElement('a');
        a.href = url;
        a.download = `${organisation!.name}_team_${dateString}_${timeString}.csv`;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        URL.revokeObjectURL(url);
        setIsExporting(false)
        setSucces('Teams succesvol geëxporteerd');
    };

    const SuccesReset = () => {
        setSucces('');
    };

    const ErrorReset = () => {
        setError('');
    };

    return (
        <>

            {showModal && (
                <div style={{
                    position: 'fixed',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    zIndex: 1000,
                }}>
                    <div style={{
                        backgroundColor: '#212232',
                        padding: '20px',
                        borderRadius: '10px',
                        position: 'relative',
                        width: '400px',
                        maxWidth: '100%',
                        textAlign: 'center',
                        border: '1px solid white',
                    }}>
                        <button
                            onClick={handleModalClose}
                            style={{
                                position: 'absolute',
                                top: '5px',
                                right: '10px',
                                background: 'none',
                                border: 'none',
                                color: 'white',
                                fontSize: '20px',
                                cursor: 'pointer',
                            }}
                        >
                            <FontAwesomeIcon icon={faTimes} />
                        </button>
                        <h2 style={{ color: 'white', marginBottom: '20px' }}>Voeg een nieuw team toe</h2>
                        <InputField
                            placeholder='Team naam'
                            value={teamName}
                            onChange={(e) => setTeamName(e.target.value)}
                            style={{ width: '400px', marginBottom: '20px' }}
                            autofocus={true}
                        />
                        <button
                            onClick={handleModalSubmit}
                            style={{
                                backgroundColor: '#4CAF50',
                                border: 'none',
                                padding: '10px 20px',
                                borderRadius: '5px',
                                color: 'white',
                                fontSize: '16px',
                                cursor: 'pointer',
                                marginRight: '10px',
                            }}
                        >
                            <FontAwesomeIcon icon={faCheck} /> Bevestigen
                        </button>
                    </div>
                </div>
            )}

            {showEditModal && (
                <div style={{
                    position: 'fixed',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    zIndex: 1000,
                }}>
                    <div style={{
                        backgroundColor: '#212232',
                        padding: '20px',
                        borderRadius: '10px',
                        position: 'relative',
                        width: '400px',
                        maxWidth: '100%',
                        textAlign: 'center',
                        border: '1px solid white',
                    }}>
                        <button
                            onClick={handleModalClose}
                            style={{
                                position: 'absolute',
                                top: '5px',
                                right: '10px',
                                background: 'none',
                                border: 'none',
                                color: 'white',
                                fontSize: '20px',
                                cursor: 'pointer',
                            }}
                        >
                            <FontAwesomeIcon icon={faTimes} />
                        </button>
                        <h2 style={{ color: 'white', marginBottom: '20px' }}>Bewerk team</h2>
                        <InputField
                            placeholder='Team naam'
                            value={teamName}
                            onChange={(e) => setTeamName(e.target.value)}
                            style={{ width: '400px', marginBottom: '20px' }}
                            autofocus={true}
                        />
                        <button
                            onClick={handleEditSubmit}
                            style={{
                                backgroundColor: '#4CAF50',
                                border: 'none',
                                padding: '10px 20px',
                                borderRadius: '5px',
                                color: 'white',
                                fontSize: '16px',
                                cursor: 'pointer',
                                marginRight: '10px',
                            }}
                        >
                            <FontAwesomeIcon icon={faCheck} /> Bevestigen
                        </button>
                    </div>
                </div>
            )}

            <div style={{
                padding: '20px',
                backgroundColor: '#212232',
                borderRadius: '20px',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                margin: '25px',
                paddingBottom: '50px',
            }}>
                <div style={{
                    marginTop: '15px',
                    display: 'flex',
                    justifyContent: 'space-between',
                    width: '100%',
                    maxWidth: '100%',
                }}>
                    <input
                        type="search"
                        placeholder="zoek team..."
                        value={filterText}
                        onChange={(e) => setFilterText(e.target.value)}
                        className='focus:outline-none'
                        style={{
                            width: '350px',
                            padding: '10px 18px',
                            background: '#2a2a3d',
                            border: '1px solid #444',
                            fontSize: '16px',
                            boxSizing: 'border-box',
                            marginRight: '10px',
                            transition: 'border-color 0.3s',
                            borderRadius: '5px',
                        }}
                    />
                    <div>
                        <Button
                            onclick={handleExport}
                            name='Exporteren'
                            isLoading={isExporting}
                            style={{ width: '150px' }}
                        />

                        <Button
                            onclick={handleAdd}
                            name='Toevoegen'
                            style={{ width: '150px', marginLeft: '30px' }}
                        />
                    </div>
                </div>
                <Error message={error} onClose={ErrorReset} style={{ width: '100%' }} />
                <Succes message={succes} onClose={SuccesReset} style={{ width: '100%' }} />

                <table style={{
                    width: '100%',
                    borderCollapse: 'collapse',
                    borderRadius: '10px',
                    boxShadow: '0 2px 10px rgba(0, 0, 0, 0.05)',
                    marginTop: '20px',
                }}>
                    <thead>
                        <tr style={{
                            color: 'white',
                        }}>
                            <th style={{
                                fontSize: '22px',
                                fontWeight: 'bold',
                                padding: '15px',
                                borderBottom: '2px solid white',
                                textAlign: 'center',
                            }} colSpan={2}>
                                Teams
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {filteredTeam.length > 0 ? (
                            filteredTeam.map((team: Team) => (
                                <tr key={team.id} style={{
                                    borderBottom: '1px solid white',
                                    transition: 'background-color 0.3s',
                                }}
                                    onMouseEnter={(e) => { e.currentTarget.style.backgroundColor = '#1e202d'; }}
                                    onMouseLeave={(e) => { e.currentTarget.style.backgroundColor = 'transparent'; }}
                                >
                                    <td style={{
                                        fontSize: '20px',
                                        color: 'white',
                                        padding: '15px 20px',
                                    }}>{team.name}</td>
                                    <td style={{
                                        padding: '15px 20px',
                                        textAlign: 'right',
                                    }}>
                                        <button onClick={() => handleView(team.id)} title="View" style={{
                                            marginRight: '20px',
                                            backgroundColor: 'transparent',
                                            border: 'none',
                                            cursor: 'pointer',
                                        }}>
                                            <FontAwesomeIcon icon={faEye} style={{ fontSize: '22px', color: '#4CAF50' }} />
                                        </button>
                                        <button onClick={() => handleEdit(team.id, team.name)} title="Edit" style={{
                                            marginRight: '20px',
                                            backgroundColor: 'transparent',
                                            border: 'none',
                                            cursor: 'pointer',
                                        }}>
                                            <FontAwesomeIcon icon={faEdit} style={{ fontSize: '20px', color: 'lightblue' }} />
                                        </button>
                                        <button onClick={() => handleDelete(team.id)} title="Delete" style={{
                                            backgroundColor: 'transparent',
                                            border: 'none',
                                            cursor: 'pointer',
                                        }}>
                                            <FontAwesomeIcon icon={faTrash} style={{ fontSize: '22px', color: 'red' }} />
                                        </button>
                                    </td>
                                </tr>
                            ))
                        ) : (
                            <tr>
                                <td colSpan={2} style={{
                                    color: 'white',
                                    textAlign: 'center',
                                    padding: '15px',
                                }}>
                                    Geen teams gevonden
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>

            <style>
                {`
                    input[type="search"]::-webkit-search-cancel-button {
                        -webkit-filter: invert(50%); /* Invert the color to make it white if background is dark */
                    }
                `}
            </style>
        </>
    );
};

export default TeamList;
