import React, { useState } from 'react';
import Card from '../shared/components/form/card';
import { faUserPlus, faUserMinus } from '@fortawesome/free-solid-svg-icons';
import { useGetdashboardApiQuery } from '../redux/services/organisation';
import { useNavigate } from 'react-router-dom';

const Dashboard = () => {
    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));

    const navigate = useNavigate();
    const { data } = useGetdashboardApiQuery(org_id!);

    return (
        <>
            <div style={{
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'space-between',
                padding: '25px 25px',
                boxSizing: 'border-box',
                gap: '25px'
            }}>
                <Card style={{ flex: '1 1 calc(25% - 50px)', minWidth: '200px' }} title="Aantal leden in de club" number={data?.totalPlayers.toString() ?? "0"} color="#04d449" icon={faUserPlus} onClick={() => navigate('/player/view')} />
                <Card style={{ flex: '1 1 calc(25% - 50px)', minWidth: '200px' }} title="Aantal leden die de club hebben verlaten" number={data?.totalPlayersDelete.toString() ?? "0"} color="red" icon={faUserMinus} onClick={() => navigate('/player/view')} />
                <Card style={{ flex: '1 1 calc(25% - 50px)', minWidth: '200px' }} title="Aantal teams in de club" number={data?.totalTeams.toString() ?? "0"} color="#04d449" icon={faUserPlus} onClick={() => navigate('/team/view')} />
                <Card style={{ flex: '1 1 calc(25% - 50px)', minWidth: '200px' }} title="Aantal gebruikers" number={data?.totalUsers.toString() ?? "0"} color="#04d449" icon={faUserPlus} onClick={() => navigate('/club/user')} />
            </div>

        </>
    );
}

export default Dashboard;
