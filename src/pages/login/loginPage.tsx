import React, { useState, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import ImageRotateAnimation from '../../shared/components/image/imageRotateAnimation';
import Error from '../../shared/components/form/error';
import backgroundImage1 from '../../assets/Dashboard.avif';
import backgroundImage2 from '../../assets/Dahsboard2.webp';
import { useLoginApiMutation } from '../../redux/services/user';
import { jwtDecode } from 'jwt-decode';

interface JwtPayload {
    org_id: string;
}

const LoginPage = () => {
    const [error, setError] = useState('');

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [showPassword, setShowPassword] = useState(false);
    const [loginApi, { isLoading }] = useLoginApiMutation();

    const emailRef = useRef<HTMLInputElement | null>(null);
    const passwordRef = useRef<HTMLInputElement | null>(null);
    const submitButtonRef = useRef<HTMLButtonElement | null>(null);

    const handleLogin = async () => {
        try {
            const result = await loginApi({ email, password }).unwrap();
            sessionStorage.setItem('token', result.token);

            const decoded = jwtDecode<JwtPayload>(result.token);
            sessionStorage.setItem('org_id', decoded.org_id);

            window.location.href = '/';
        } catch (err) {
            setError('wachtwoord of email is verkeerd ingevuld');
        }
    };

    React.useEffect(() => {
        document.body.style.overflow = 'hidden';
        return () => {
            document.body.style.overflow = 'auto';
        };
    }, []);

    const ErrorReset = () => {
        setError('');
    };

    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };

    const images = [backgroundImage1, backgroundImage2, backgroundImage1, backgroundImage2, backgroundImage1];

    const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>, nextRef: React.RefObject<HTMLInputElement | HTMLButtonElement>) => {
        if (e.key === 'Enter' && nextRef.current) {
            nextRef.current.focus();
        }
    };

    return (
        <div style={{ display: 'flex', height: '100vh', width: '100vw' }}>
            <div style={{ width: '30vw', background: '#212232', display: 'flex', justifyContent: 'center', alignItems: 'center', flexGrow: 1 }}>

                <div style={{ width: '80%', textAlign: 'center' }} className='p-8'>
                    <h1 style={{ fontSize: '40px', fontWeight: 'bold', marginBottom: '30px' }}>Inloggen in AFA Sports</h1>
                    <Error message={error} onClose={ErrorReset} />
                    <label htmlFor="email" style={{ display: 'block', color: 'white', fontSize: '16px', marginBottom: '4px', textAlign: 'left' }}>Email</label>
                    <input
                        type="text"
                        placeholder="Email"
                        className='p-2 px-4 focus:outline-none'
                        style={{ display: 'block', marginBottom: '20px', width: '100%', color: 'white', border: '1px solid white', backgroundColor: '#212232' }}
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        ref={emailRef}
                        autoFocus
                        onKeyDown={(e) => handleKeyDown(e, passwordRef)}
                        tabIndex={1}
                    />

                    <label htmlFor="password" style={{ display: 'block', color: 'white', fontSize: '16px', marginBottom: '4px', textAlign: 'left' }}>Password</label>
                    <div style={{ position: 'relative' }}>
                        <input
                            type={showPassword ? 'text' : 'password'}
                            placeholder="Password"
                            className='p-2 px-4 focus:outline-none'
                            style={{ display: 'block', width: '100%', color: 'white', border: '1px solid white', backgroundColor: '#212232' }}
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            ref={passwordRef}
                            onKeyDown={(e) => handleKeyDown(e, submitButtonRef)}
                            tabIndex={2}
                        />
                        <span
                            onClick={togglePasswordVisibility}
                            onKeyDown={(e) => {
                                if (e.key === 'Enter' || e.key === ' ') {
                                    togglePasswordVisibility();
                                }
                            }}
                            role="button"
                            aria-label={showPassword ? "Hide password" : "Show password"}
                            tabIndex={4}
                            style={{ position: 'absolute', right: '10px', top: '50%', transform: 'translateY(-50%)', cursor: 'pointer', color: 'white' }}
                        >
                            <FontAwesomeIcon icon={showPassword ? faEyeSlash : faEye} />
                        </span>
                    </div>

                    <button
                        type="submit"
                        onClick={handleLogin}
                        ref={submitButtonRef}
                        disabled={isLoading}
                        className="border border-white text-white hover:border-gray-300 hover:text-gray-300 transition duration-200"
                        style={{
                            width: '200px',
                            padding: '12px',
                            borderRadius: '50px',
                            background: 'transparent',
                            fontSize: '18px',
                            boxSizing: 'border-box',
                            marginLeft: '10px',
                            cursor: 'pointer',
                            marginBottom: '25px',
                            marginTop: '25px'
                        }}
                        tabIndex={3}
                    >
                        inloggen
                    </button>
                    <br />
                    <a
                        href="#"
                        style={{ fontSize: '16px', cursor: 'pointer', color: 'white' }}
                        className="hover:underline focus:outline-none focus:ring-2 focus:ring-blue-500"
                        tabIndex={5}
                    >
                        Wachtwoord vergeten?
                    </a>
                    <br />
                    <a
                        href="#"
                        style={{ fontSize: '16px', cursor: 'pointer', color: 'white' }}
                        className="hover:underline focus:outline-none focus:ring-2 focus:ring-blue-500"
                        tabIndex={6}
                    >
                        Geen account?
                    </a>
                </div>
            </div>

            <div className="image-container" style={{ flexGrow: 1, overflow: 'hidden', position: 'relative', maxHeight: '100vh', width: '70vw' }}>
                <ImageRotateAnimation images={images} />
            </div>

            <style>{`
                @media (max-width: 1250px) {
                    .image-container {
                        display: none;
                    }
                }

                @media (min-width: 2500px) {
                    .image-container {
                        display: none;
                    }
                }
            `}
            </style>
        </div>
    );
};

export default LoginPage;
