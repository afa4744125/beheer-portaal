import React, { useState } from 'react';
import Datagrid from '../../shared/components/datagrid/Datagrid';
import PlayerFilter from './playerFilter'
import type { Filter } from '../../redux/types/player';
const PlayerList = () => {

    const [filters, setFilters] = useState<Filter>({});

    const handleApplyFilters = (newFilters: Filter) => {
        setFilters(newFilters);
    };

    return (
        <>
            <PlayerFilter onApplyFilters={handleApplyFilters} />
            <Datagrid filter={filters} />
        </>
    );
}

export default PlayerList;
