import React, { useState } from 'react';
import { processCSV } from '../../shared/components/dataProcessors/ImportData'; // Importeren van de helperfunctie
import { importPlayer, importRequest } from '../../redux/types/player';
import { useImportPlayerApiMutation } from '../../redux/services/player';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import Button from '../../shared/components/form/button';
import Succes from '../../shared/components/form/succes';
import Error from '../../shared/components/form/error';

const PlayerImport = () => {
    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));
    const [fileData, setFileData] = useState<importPlayer[] | null>(null);
    const [error, setError] = useState('');
    const [succes, setSucces] = useState('');
    const [currentPage, setCurrentPage] = useState(1);
    const [itemsPerPage] = useState(20);

    const [ImportPlayers, { isLoading }] = useImportPlayerApiMutation();

    const handleDownload = () => {
        const link = document.createElement('a');
        link.href = '/exampleImport.csv';
        link.download = 'exampleImport.csv';
        link.click();
    };

    const handleFileUpload = async (data: string, fileExtension: string) => {
        if (fileExtension === 'csv') {
            const { players, errors } = processCSV(data);
            if (errors.length === 0) {
                setFileData(players);
            }
            setError(errors.join('\n'))
            setCurrentPage(1);
            if (!fileData || !org_id) return;
        } else {
            setError('Alleen CSV bestanden zijn toegestaan.');
        }
    };

    const handleImport = async () => {
        if (!fileData || !org_id) return;

        const importRequestData: importRequest = {
            org_id,
            player: fileData,
        };

        try {
            await ImportPlayers(importRequestData).unwrap();
            setFileData(null)
            setSucces('de spelers zijn succesvol geimporteerd');
        } catch (err) {
            setError('Er is iets misgegaan tijdens het importeren van de spelers.');
        }
    };

    const indexOfLastPlayer = Math.min((fileData?.length ?? 0), currentPage * itemsPerPage);
    const indexOfFirstPlayer = 0 + ((currentPage - 1) * itemsPerPage);
    const currentPlayers = fileData ? fileData.slice(indexOfFirstPlayer, indexOfLastPlayer) : [];

    const nextPage = () => {
        if (fileData && currentPage < Math.ceil(fileData.length / itemsPerPage)) {
            setCurrentPage(currentPage + 1);
        }
    };

    const prevPage = () => {
        if (currentPage > 1) {
            setCurrentPage(currentPage - 1);
        }
    };

    const SuccesReset = () => {
        setSucces('');
    };

    const ErrorReset = () => {
        setError('');
    };

    return (
        <>
            <div
                style={{
                    padding: '20px',
                    backgroundColor: '#212232',
                    borderRadius: '20px',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    margin: '25px',

                }}
            >
                <h1 style={{ fontSize: '34px', marginBottom: '20px' }}>Spelers importeren uitleg</h1>
                <a style={{ fontSize: '20px', marginBottom: '10px' }}>
                    Hier kun je spelers importeren voor je club. De laatste geüpdatete versie is op 1 december 2024. <br />
                    Wil je meer weten hoe het importbestand eruit ziet? Klik dan op voorbeeld import.
                </a>

                <Button
                    name='Voorbeeld Import'
                    style={{ marginTop: '20px' }}
                    onclick={handleDownload}
                />
            </div>

            <div
                style={{
                    padding: '20px',
                    backgroundColor: '#212232',
                    borderRadius: '20px',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    margin: '25px',
                }}
            >

                <h1 style={{ fontSize: '27px', marginBottom: '20px' }}>Spelers importeren uitleg</h1>
                <Error message={error} onClose={ErrorReset} style={{ width: '100%' }} />
                <Succes message={succes} onClose={SuccesReset} style={{ width: '100%' }} />
                {!fileData && (
                    <input
                        type="file"
                        accept=".csv"
                        onChange={(e) => {
                            const file = e.target.files ? e.target.files[0] : null;
                            if (file) {
                                const reader = new FileReader();
                                reader.onload = (event) => {
                                    const data = event.target?.result as string;
                                    if (data) {
                                        handleFileUpload(data, 'csv');
                                    }
                                };
                                reader.readAsText(file);
                            }
                        }}
                        style={{
                            padding: '10px',
                            backgroundColor: '#333',
                            borderRadius: '5px',
                            color: '#fff',
                            marginBottom: '20px',
                        }}
                    />
                )}
                {fileData && (
                    <>
                        <table
                            style={{
                                width: '100%',
                                borderCollapse: 'collapse',
                                borderRadius: '10px',
                                boxShadow: '0 2px 10px rgba(0, 0, 0, 0.05)',
                                marginTop: '20px',
                                tableLayout: 'fixed',
                            }}
                        >
                            <thead>
                                <tr style={{ color: 'white' }}>
                                    <th
                                        style={{
                                            fontSize: '20px',
                                            fontWeight: 'bold',
                                            borderBottom: '2px solid white',
                                            textAlign: 'left',
                                            padding: '15px 0',
                                            paddingLeft: '20px',
                                        }}
                                        colSpan={1}
                                    >
                                        Lidnummer
                                    </th>
                                    <th
                                        style={{
                                            fontSize: '20px',
                                            fontWeight: 'bold',
                                            borderBottom: '2px solid white',
                                            textAlign: 'left',
                                            padding: '15px 0'
                                        }}
                                        colSpan={1}
                                    >
                                        Naam
                                    </th>
                                    <th
                                        style={{
                                            fontSize: '20px',
                                            fontWeight: 'bold',
                                            borderBottom: '2px solid white',
                                            textAlign: 'left',
                                            padding: '15px 0'
                                        }}
                                        colSpan={1}
                                    >
                                        Geslacht
                                    </th>
                                    <th
                                        style={{
                                            fontSize: '20px',
                                            fontWeight: 'bold',
                                            borderBottom: '2px solid white',
                                            textAlign: 'left',
                                            padding: '15px 0'
                                        }}
                                        colSpan={1}
                                    >
                                        Relatienummer
                                    </th>
                                    <th
                                        style={{
                                            fontSize: '20px',
                                            fontWeight: 'bold',
                                            borderBottom: '2px solid white',
                                            textAlign: 'left',
                                            padding: '15px 0'
                                        }}
                                        colSpan={1}
                                    >
                                        Team
                                    </th>
                                    <th
                                        style={{
                                            fontSize: '20px',
                                            fontWeight: 'bold',
                                            borderBottom: '2px solid white',
                                            textAlign: 'left',
                                            padding: '15px 0'
                                        }}
                                        colSpan={1}
                                    >
                                        Geboortedatum
                                    </th>
                                    <th
                                        style={{
                                            fontSize: '20px',
                                            fontWeight: 'bold',
                                            borderBottom: '2px solid white',
                                            textAlign: 'left',
                                            padding: '15px 0'
                                        }}
                                        colSpan={1}
                                    >
                                        Email
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {currentPlayers.map((player, index) => (
                                    <tr
                                        key={index}
                                        style={{
                                            borderBottom: '1px solid white',
                                            transition: 'background-color 0.3s',
                                        }}
                                        onMouseEnter={(e) => { e.currentTarget.style.backgroundColor = '#1e202d'; }}
                                        onMouseLeave={(e) => { e.currentTarget.style.backgroundColor = 'transparent'; }}
                                    >
                                        <td
                                            style={{
                                                fontSize: '20px',
                                                color: 'white',
                                                padding: '15px 0',
                                                paddingLeft: '20px',
                                            }}
                                        >
                                            {player.membershipnumber}
                                        </td>
                                        <td
                                            style={{
                                                fontSize: '20px',
                                                color: 'white',
                                                padding: '15px 0'

                                            }}
                                        >
                                            {player.firstname} {player.infix} {player.lastname}
                                        </td>
                                        <td
                                            style={{
                                                fontSize: '20px',
                                                color: 'white',
                                                padding: '15px 0'
                                            }}
                                        >
                                            {player.gender === "man" ? "man" : "vrouw"}
                                        </td>
                                        <td
                                            style={{
                                                fontSize: '20px',
                                                color: 'white',
                                                padding: '15px 0'
                                            }}
                                        >
                                            {player.relationshipnumber}
                                        </td>
                                        <td
                                            style={{
                                                fontSize: '20px',
                                                color: 'white',
                                                padding: '15px 0'
                                            }}
                                        >
                                            {player.team}
                                        </td>
                                        <td
                                            style={{
                                                fontSize: '20px',
                                                color: 'white',
                                                padding: '15px 0'
                                            }}
                                        >
                                            {player.dateofbirth}
                                        </td>
                                        <td
                                            style={{
                                                fontSize: '20px',
                                                color: 'white',
                                                padding: '15px 0'
                                            }}
                                        >
                                            {player.email}
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>

                        <div style={{ marginTop: '20px', textAlign: 'center' }}>
                            <button
                                onClick={prevPage}
                                disabled={currentPage === 1}
                                style={{
                                    padding: '10px 20px',
                                    color: '#fff',
                                    borderRadius: '5px',
                                    cursor: currentPage === 1 ? 'not-allowed' : 'pointer',
                                }}
                            >
                                <FontAwesomeIcon icon={faChevronLeft} style={{ fontSize: '18px' }} />
                            </button>
                            <a style={{ color: '#fff', fontSize: '20px', fontWeight: 'bold' }}> {indexOfFirstPlayer} / {indexOfLastPlayer} of {fileData.length ?? 0}</a>
                            <button
                                onClick={nextPage}
                                disabled={currentPage === Math.ceil((fileData?.length || 0) / itemsPerPage)}
                                style={{
                                    padding: '10px 20px',
                                    color: '#fff',
                                    borderRadius: '5px',
                                    cursor: currentPage === Math.ceil((fileData?.length || 0) / itemsPerPage) ? 'not-allowed' : 'pointer',
                                }}
                            >
                                <FontAwesomeIcon icon={faChevronRight} style={{ fontSize: '18px' }} />
                            </button>
                        </div>

                        <Button
                            name={isLoading ? 'Importeer Alle Spelers...' : ' Importeer Alle Spelers '}
                            style={{ marginTop: '20px' }}
                            onclick={handleImport}
                        />
                    </>
                )}
            </div>
        </>
    );
};

export default PlayerImport;
