import React, { useState } from 'react';
import validator from 'validator';
import InputField from '../../shared/components/form/inputfield';
import Dropdown from '../../shared/components/form/dropdown';
import DatePicker from '../../shared/components/form/datePicker';
import Button from '../../shared/components/form/button';
import { useTeamsApiQuery } from '../../redux/services/team'
import { useCreatePlayerApiMutation } from '../../redux/services/player';
import { useNavigate } from 'react-router-dom';
import Succes from '../../shared/components/form/succes';
import Error from '../../shared/components/form/error';
import type { Team } from '../../redux/types/team';
import type { CreatePlayer, Gender } from '../../redux/types/player';


const PlayerAdd = () => {

    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));

    const Genders = [
        { id: 'man', name: 'man' },
        { id: 'woman', name: 'vrouw' }
    ];

    const [firstname, setFirstname] = useState<string>('');
    const [infix, setInfix] = useState<string | null>(null);
    const [lastname, setLastname] = useState<string>('');
    const [membershipnumber, setMembershipnumber] = useState<string>('');
    const [relationshipnumber, setRelationshipnumber] = useState<string | null>(null);
    const [email, setEmail] = useState<string | null>(null)
    const [gender, setGender] = useState(Genders[0]);
    const [team, setTeam] = useState<Team>();
    const [dateofbirth, setDateofbirth] = useState<string>('')

    const { data } = useTeamsApiQuery(org_id || '', { skip: !org_id });

    const [error, setError] = useState('');
    const [succes, setSucces] = useState('');

    const navigate = useNavigate();

    const [CreatePlayer, { isLoading }] = useCreatePlayerApiMutation();

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            if (org_id) {
                if (email !== '' && email !== null) {
                    if (!validator.isEmail(email)) {
                        setError('Ongeldig e-mailadres');
                        return;
                    }
                }

                const player: CreatePlayer = {
                    org_id,
                    firstname,
                    infix: infix || null,
                    lastname,
                    membershipnumber,
                    relationshipnumber: relationshipnumber || null,
                    dateofbirth,
                    email: email || null,
                    team_id: team?.id ?? null,
                    gender: gender.id as Gender
                };

                await CreatePlayer(player).unwrap();
                setSucces('Succesvol create de speler');

                setTimeout(() => {
                    navigate('/player/view');
                }, 2000);
            }
        } catch (err) {
            setError('Niet gelukt om de speler toetevoegen');
        }
    };

    const SuccesReset = () => {
        setSucces('');
    };

    const ErrorReset = () => {
        setError('');
    };

    //@ts-ignore
    const Teams: Team[] = data
        ? [{ id: null, name: "Geen Team" }, ...data.map((item: Team) => ({ id: item.id, name: item.name }))]
        : [];

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                margin: '30px',
                height: '100%',
            }}
        >
            <div
                style={{
                    padding: '30px',
                    backgroundColor: '#212232',
                    borderRadius: '15px',
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                    margin: '0 auto',
                    width: '100%',
                    maxWidth: '650px',
                    boxShadow: '0 4px 12px rgba(0, 0, 0, 0.3)',
                    boxSizing: 'border-box',
                    transition: 'all 0.3s ease',
                }}>
                <h1 style={{ fontSize: '34px', marginBottom: '40px' }}>Speler Aanmaken</h1>
                <Error message={error} onClose={ErrorReset} style={{ width: '100%' }} />
                <Succes message={succes} onClose={SuccesReset} style={{ width: '100%' }} />
                <form onSubmit={handleSubmit} style={{ width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    <InputField
                        label='Voornaam'
                        placeholder='Voornaam'
                        value={firstname}
                        onChange={(e) => setFirstname(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                        required
                    />
                    <InputField
                        label='Tussenvoegsel'
                        placeholder='Tussenvoegsel'
                        value={infix}
                        onChange={(e) => setInfix(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                    />

                    <InputField
                        label='Achternaam'
                        placeholder='Achternaam'
                        value={lastname}
                        onChange={(e) => setLastname(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                        required
                    />

                    <InputField
                        label='Lidnummer'
                        placeholder='Lidnummer'
                        value={membershipnumber}
                        onChange={(e) => setMembershipnumber(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                        required
                    />

                    <InputField
                        label='Relatienummer'
                        placeholder='Relatienummer'
                        value={relationshipnumber}
                        onChange={(e) => setRelationshipnumber(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                    />

                    <InputField
                        label='Email'
                        placeholder='Email'
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                    />

                    <DatePicker
                        label='Geboortedatum'
                        value={dateofbirth}
                        onChange={(e) => setDateofbirth(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                        required
                    />

                    <Dropdown
                        style={{ width: '600px', marginBottom: '30px' }}
                        label='Geslacht'
                        value={gender?.id}
                        options={Genders}
                        noDefaultSearch
                        onChange={(selectedId) => {
                            const selectedGender = Genders.find((r) => r.id === selectedId);
                            if (selectedGender) {
                                setGender(selectedGender);
                            }
                        }}
                    />

                    <Dropdown
                        style={{ width: '600px', marginBottom: '30px' }}
                        label='Team'
                        placeholder='Selecteer een team'
                        value={team?.id}
                        options={Teams}
                        noDefaultSearch
                        onChange={(selectedId) => {
                            const selectedTeam = Teams.find((r) => r.id === selectedId);
                            if (selectedTeam) {
                                setTeam(selectedTeam);
                            }
                        }}
                    />

                    <Button
                        name='Toevoegen'
                        isLoading={isLoading}
                        style={{ width: '150px' }}
                    />

                </form>
            </div>
        </div>
    );
};

export default PlayerAdd;
