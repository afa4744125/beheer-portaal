import React, { useEffect, useState } from 'react';
import InputField from '../../shared/components/form/inputfield';
import Dropdown from '../../shared/components/form/dropdown';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons';
import { useTeamsApiQuery } from '../../redux/services/team'
import type { Team } from '../../redux/types/team';
import type { Filter } from '../../redux/types/player';
interface filterProps {
    onApplyFilters: (filters: Filter) => void;
}

const PlayerFilter: React.FC<filterProps> = ({ onApplyFilters }) => {

    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));
    const [team_id, setTeam_id] = useState<string | null>(null);

    useEffect(() => {
        const params = new URLSearchParams(location.search);
        const teamIdFromUrl = params.get("team_id");
        setTeam_id(teamIdFromUrl);
    }, [location]);

    const Genders = [
        { id: '', name: 'geen gender' },
        { id: 'man', name: 'man' },
        { id: 'woman', name: 'vrouw' },
    ]

    useEffect(() => {
        onApplyFilters({
            team_id: team_id ?? '',
        });
        setFilters((prev) => ({
            ...prev,
            team_id: team_id ?? ''
        }));
    }, [team_id]);

    const currentYear = new Date().getFullYear();
    const years = [
        { id: '', name: 'geen geboorteyear' },
        ...Array.from({ length: 101 }, (_, i) => ({
            id: (currentYear - i).toString(),
            name: (currentYear - i).toString(),
        })),
    ];

    const [filters, setFilters] = useState<Filter>({});
    const [open, setOpen] = useState(false);

    const { data: teams } = useTeamsApiQuery(org_id || '', { skip: !org_id });

    const handleFilterChange = (field: string, value: string) => {
        setFilters((prev) => ({
            ...prev,
            [field]: value,
        }));
        onApplyFilters({
            ...filters,
            [field]: value,
        });
    };

    //@ts-ignore
    const Teams: Team[] = teams
        ? [{ id: null, name: "Geen Team" }, ...teams.map((item: Team) => ({ id: item.id, name: item.name }))]
        : [];

    return (
        <div
            style={{
                margin: '25px',
                borderRadius: '30px',
                padding: '20px',
                backgroundColor: '#212232',
                boxShadow: '1',
                cursor: open ? '' : 'pointer',
            }}
            onClick={() => open ? '' : setOpen(true)}
        >
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '0px 10px' }}>
                <h2 style={{ color: '#fff', margin: 0, fontSize: 20 }}>Zoek Speler</h2>
                <FontAwesomeIcon
                    icon={open ? faChevronDown : faChevronUp}
                    style={{ color: '#fff', cursor: 'pointer', fontSize: 20 }}
                    onClick={() => setOpen(!open)}
                />
            </div>
            {open && (
                <div
                    style={{
                        display: 'grid',
                        gridTemplateColumns: 'repeat(4, 1fr)',
                        gap: '20px',
                        marginTop: '20px',
                        borderTop: '1px solid',
                        paddingTop: '20px'
                    }}
                >
                    <InputField
                        label="Lidnummer"
                        placeholder="Lidnummer"
                        value={filters.membershipnumber}
                        onChange={(e) => handleFilterChange('membershipnumber', e.target.value)}
                    />

                    <InputField
                        label="Voornaam"
                        placeholder="Voornaam"
                        value={filters.firstname}
                        onChange={(e) => handleFilterChange('firstname', e.target.value)}
                    />

                    <InputField
                        label="Achternaam"
                        placeholder="Achternaam"
                        value={filters.lastname}
                        onChange={(e) => handleFilterChange('lastname', e.target.value)}
                    />

                    <InputField
                        label="Email"
                        placeholder="Email"
                        value={filters.email}
                        onChange={(e) => handleFilterChange('email', e.target.value)}
                    />

                    <InputField
                        label="Relatienummer"
                        placeholder="Relatienummer"
                        value={filters.relationshipnumber}
                        onChange={(e) => handleFilterChange('relationshipnumber', e.target.value)}
                    />

                    <Dropdown
                        style={{ width: '100%' }}
                        label='Team'
                        value={filters.team_id ?? ''}
                        options={Teams}
                        noDefaultSearch
                        placeholder='selecteer team'
                        onChange={(selectedId) => {
                            const selectedTeam = Teams.find((r) => r.id === selectedId);
                            if (selectedTeam) {
                                handleFilterChange('team_id', selectedTeam.id)
                            }
                        }}
                    />

                    <Dropdown
                        style={{ width: '100%' }}
                        label='Gender'
                        value={filters.gender ?? ''}
                        options={Genders}
                        noDefaultSearch
                        placeholder='selecteer gender'
                        onChange={(selectedId) => {
                            const selectedGender = Genders.find((r) => r.id === selectedId);
                            if (selectedGender) {
                                handleFilterChange('gender', selectedGender.id)
                            }
                        }}
                    />

                    <Dropdown
                        style={{ width: '100%' }}
                        label='Geboortejaar'
                        value={filters.dateofbirth ?? ''}
                        options={years}
                        noDefaultSearch
                        placeholder='Geboortejaar'
                        onChange={(selectedId) => {
                            const selectedDateofbirth = years.find((r) => r.id === selectedId);
                            if (selectedDateofbirth) {
                                handleFilterChange('dateofbirth', selectedDateofbirth.id)
                            }
                        }}
                    />
                </div>
            )}
        </div>
    );
};

export default PlayerFilter;