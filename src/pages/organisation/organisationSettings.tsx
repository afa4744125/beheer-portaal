import React, { useEffect, useState } from 'react';
import validator from 'validator';
import InputField from '../../shared/components/form/inputfield';
import Button from '../../shared/components/form/button';
import Succes from '../../shared/components/form/succes';
import Error from '../../shared/components/form/error';
import { useSetSettingsApiMutation, useGetSettingsApiQuery } from '../../redux/services/organisation';

const OrganisationSettings = () => {

    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));

    const [email, setEmail] = useState('');

    const [error, setError] = useState('');
    const [succes, setSucces] = useState('');

    const { data: settings, refetch } = useGetSettingsApiQuery(org_id!);
    const [updateSettings, { isLoading }] = useSetSettingsApiMutation();

    useEffect(() => {
        if (settings) {
            setEmail(settings.email);
        }
    }, [settings]);

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            if (org_id) {
                if (!validator.isEmail(email)) {
                    setError('Ongeldig e-mailadres');
                    return;
                }

                const settings = {
                    org_id,
                    email: email,
                };

                await updateSettings(settings).unwrap();

                setSucces('Succesvol update club instelling');

                refetch();
            }
        } catch (err) {
            setError('Failed to update club instelling');
        }
    };

    const SuccesReset = () => {
        setSucces('');
    };

    const ErrorReset = () => {
        setError('');
    };

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                margin: '30px',
                height: '100%',
            }}
        >
            <div
                style={{
                    padding: '30px',
                    backgroundColor: '#212232',
                    borderRadius: '15px',
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                    margin: '0 auto',
                    width: '100%',
                    maxWidth: '650px',
                    boxShadow: '0 4px 12px rgba(0, 0, 0, 0.3)',
                    boxSizing: 'border-box',
                    transition: 'all 0.3s ease',
                }}>
                <h1 style={{ fontSize: '34px', marginBottom: '40px' }}>Club instelling</h1>
                <Error message={error} onClose={ErrorReset} style={{ width: '100%' }} />
                <Succes message={succes} onClose={SuccesReset} style={{ width: '100%' }} />
                <form onSubmit={handleSubmit} style={{ width: '100%', height: 'auto', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    <InputField
                        label='Club Email'
                        placeholder='Email'
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        style={{ width: '600px', marginBottom: '30px' }}
                        required
                    />

                    <Button
                        name='Opslaan'
                        isLoading={isLoading}
                        style={{ width: '150px' }}
                    />

                </form>
            </div>
        </div >
    );
};

export default OrganisationSettings;
