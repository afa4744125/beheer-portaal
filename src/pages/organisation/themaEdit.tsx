import React, { useState, useEffect, ChangeEvent } from 'react';
import ColorPicker from '../../shared/components/form/colorPicker';
import FilePicker from '../../shared/components/form/filePicker';
import Button from '../../shared/components/form/button';
import Banner from '../../shared/components/image/banner';
import Outfit from '../../shared/components/image/outfit';
import { useSetThemaApiMutation, useGetThemaApiQuery } from '../../redux/services/organisation';
import AfaLogo from '../../assets/AfaLogobg.png';
import Succes from '../../shared/components/form/succes';
import Error from '../../shared/components/form/error';

const ThemaEdit = () => {
    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));

    const { data: thema } = useGetThemaApiQuery(org_id!);

    const [primaryColor, setPrimaryColor] = useState(thema?.primaryColor ?? '#ffffff');
    const [secondaryColor, setSecondaryColor] = useState(thema?.secondaryColor ?? '#ffffff');
    const [tertiaryColor, setTertiaryColor] = useState(thema?.tertiaryColor ?? '#000000');
    const [logo, setLogo] = useState('');

    const [currentIndex, setCurrentIndex] = useState(0);
    const [transitioning, setTransitioning] = useState(false);

    const [setThema, { isLoading }] = useSetThemaApiMutation();

    const [error, setError] = useState('');
    const [succes, setSucces] = useState('');

    const components = [
        <Outfit
            primaryColor={primaryColor}
            secondaryColor={secondaryColor}
            tertiaryColor={tertiaryColor}
            logo={logo || thema?.logo || AfaLogo}
        />,
        <Banner
            logo={logo || thema?.logo || AfaLogo}
            primaryColor={primaryColor}
            secondaryColor={secondaryColor}
        />
    ];

    useEffect(() => {
        if (thema) {
            setPrimaryColor(thema.primaryColor ?? '#ffffff');
            setSecondaryColor(thema.secondaryColor ?? '#ffffff');
            setTertiaryColor(thema.tertiaryColor ?? '#000000');
        }
    }, [thema]);

    const handleFileChange = (event: ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];
        if (file) {
            const validTypes = ['image/jpeg', 'image/png'];

            if (!validTypes.includes(file.type)) {
                setError('Bestandstype is ongeldig. Kies een JPG of JPEG of PNG afbeelding.');
                return;
            }

            const reader = new FileReader();
            reader.onloadend = () => {
                setLogo(reader.result as string);
            };
            reader.readAsDataURL(file);
        }
    };


    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            const themas = {
                org_id: org_id!,
                primaryColor,
                secondaryColor,
                tertiaryColor,
                ...(logo && { logo }),
            };
            await setThema(themas).unwrap();
            setSucces('Succesvol bijgewerkt: club thema');

            setTimeout(() => {
                window.location.reload();
            }, 2000);

        } catch (err) {
            setError('Niet gelukt om de thema van de club updaten');
        }
    };

    const handlePrev = () => {
        setTransitioning(true);
        setTimeout(() => {
            setCurrentIndex((prevIndex) => (prevIndex === 0 ? components.length - 1 : prevIndex - 1));
            setTransitioning(false);
        }, 300);
    };

    const handleNext = () => {
        setTransitioning(true);
        setTimeout(() => {
            setCurrentIndex((prevIndex) => (prevIndex === components.length - 1 ? 0 : prevIndex + 1));
            setTransitioning(false);
        }, 300);
    };

    const SuccesReset = () => {
        setSucces('');
    };

    const ErrorReset = () => {
        setError('');
    };
    return (
        <div
            style={{
                display: 'flex',
                gap: '25px',
                marginTop: '25px',
                marginBottom: '25px',
            }}
        >
            <div
                style={{
                    padding: '20px',
                    backgroundColor: '#212232',
                    borderRadius: '20px',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    marginLeft: '25px',
                    paddingBottom: '50px',
                    width: '100%',
                }}
            >
                <Error message={error} onClose={ErrorReset} style={{ width: '100%' }} />
                <Succes message={succes} onClose={SuccesReset} style={{ width: '100%' }} />
                <h1 style={{ fontSize: '34px', marginBottom: '40px', marginTop: '20px' }}>Club Thema</h1>
                <form onSubmit={handleSubmit} style={{ width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    <ColorPicker
                        label='Primary kleur'
                        value={primaryColor}
                        onChange={(e) => setPrimaryColor(e.target.value)}
                        style={{ width: '100%', marginBottom: '30px', maxWidth: '500px' }}
                    />
                    <ColorPicker
                        label='Secondary kleur'
                        value={secondaryColor}
                        onChange={(e) => setSecondaryColor(e.target.value)}
                        style={{ width: '100%', marginBottom: '30px', maxWidth: '500px' }}
                    />
                    <ColorPicker
                        label='Broek kleur'
                        value={tertiaryColor}
                        onChange={(e) => setTertiaryColor(e.target.value)}
                        style={{ width: '100%', marginBottom: '30px', maxWidth: '500px' }}
                    />

                    <FilePicker
                        onChange={handleFileChange}
                        name='upload logo'
                        style={{ marginBottom: '30px' }}
                    />

                    <Button
                        name='Opslaan'
                        isLoading={isLoading}
                        style={{ width: '150px' }}
                    />
                </form>
            </div>
            <div
                style={{
                    backgroundColor: '#212232',
                    borderRadius: '20px',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    marginRight: '25px',
                    padding: '25px',
                    width: '100%',
                }}
            >
                <div style={{
                    width: '100%',
                    height: '100%',
                    borderRadius: '20px',
                    backgroundColor: '#2a2a3d',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}>

                    <button
                        onClick={handlePrev}
                        style={{
                            cursor: 'pointer',
                            padding: '10px',
                            background: 'transparent',
                            border: 'none',
                            color: '#fff',
                            fontSize: '24px',
                        }}
                        onMouseEnter={(e) => {
                            e.currentTarget.style.color = 'gray';
                        }}
                        onMouseLeave={(e) => {
                            e.currentTarget.style.color = '#fff';
                        }}
                    >
                        &#9664;
                    </button>
                    <div className={`transition-container ${transitioning ? 'fade' : ''}`}>
                        {components[currentIndex]}
                    </div>
                    <button
                        onClick={handleNext}
                        style={{
                            cursor: 'pointer',
                            padding: '10px',
                            background: 'transparent',
                            border: 'none',
                            color: '#fff',
                            fontSize: '24px',
                        }}
                        onMouseEnter={(e) => {
                            e.currentTarget.style.color = 'gray';
                        }}
                        onMouseLeave={(e) => {
                            e.currentTarget.style.color = '#fff';
                        }}
                    >
                        &#9654;
                    </button>
                </div>
            </div>
        </div>
    );
};

export default ThemaEdit;
