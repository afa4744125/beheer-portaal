import React, { useState } from 'react';
import validator from 'validator';
import InputField from '../../shared/components/form/inputfield';
import Button from '../../shared/components/form/button';
import { useCreateOrganisationApiMutation } from '../../redux/services/organisation';
import { useNavigate } from 'react-router-dom';
import Succes from '../../shared/components/form/succes';
import Error from '../../shared/components/form/error';

const OrganisationAdd = () => {

    const [organisationName, setOrganisationName] = useState('');
    const [organisationEmail, setOrganisationEmail] = useState('');

    const [error, setError] = useState('');
    const [succes, setSucces] = useState('');

    const navigate = useNavigate();

    const [createOrganisation, { isLoading }] = useCreateOrganisationApiMutation();


    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            if (!validator.isEmail(organisationEmail)) {
                setError('Ongeldig e-mailadres');
                return;
            }
            await createOrganisation({ name: organisationName, email: organisationEmail }).unwrap();
            navigate('/admin/club');
        } catch (err) {
            console.error('Failed to create organisation: ', err);
        }
    };

    const SuccesReset = () => {
        setSucces('');
    };

    const ErrorReset = () => {
        setError('');
    };

    return (
        <div
            style={{
                padding: '20px',
                backgroundColor: '#212232',
                borderRadius: '20px',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                margin: '25px',
                paddingBottom: '50px',
            }}
        >
            <h1 style={{ fontSize: '34px', marginBottom: '40px' }}>Club Aanmaken</h1>
            <Error message={error} onClose={ErrorReset} style={{ width: '100%' }} />
            <Succes message={succes} onClose={SuccesReset} style={{ width: '100%' }} />
            <form onSubmit={handleSubmit} style={{ width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <InputField
                    label='Club Name'
                    placeholder='Naam'
                    value={organisationName}
                    onChange={(e) => setOrganisationName(e.target.value)}
                    style={{ width: '600px', marginBottom: '30px' }}
                    required
                />
                <InputField
                    label='Organisation Email'
                    placeholder='Email'
                    value={organisationEmail}
                    onChange={(e) => setOrganisationEmail(e.target.value)}
                    style={{ width: '600px', marginBottom: '30px' }}
                    required
                />

                <Button
                    name='Toevoegen'
                    isLoading={isLoading}
                    style={{ width: '150px' }}
                />

            </form>
        </div>
    );
};

export default OrganisationAdd;
