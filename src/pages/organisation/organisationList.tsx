import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button from '../../shared/components/form/button';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { usePublicListApiQuery, useDeleteOrganisationApiMutation } from '../../redux/services/organisation';
import type { Organisations } from '../../redux//types/organisation';
import { useNavigate } from 'react-router-dom';

const OrganisationList = () => {
    const [filterText, setFilterText] = useState('');
    const { data, refetch } = usePublicListApiQuery();
    const [deleteOrganisation] = useDeleteOrganisationApiMutation();

    const navigate = useNavigate();

    const filteredOrganisations: Organisations[] =
        Array.isArray(data) ? data.filter((org: Organisations) =>
            org.name.toLowerCase().includes(filterText.toLowerCase())
        ) : [];

    const handleEdit = (id: string) => {
        navigate(`/admin/edit/club/${id}`);
    };

    const handleDelete = async (id: string) => {
        const confirmDelete = window.confirm('Are you sure you want to delete this club?');
        if (confirmDelete) {
            await deleteOrganisation(id).unwrap();
            refetch();
        }
    };

    const handleAdd = () => {
        navigate('/admin/add/club');
    };

    return (
        <div style={{
            padding: '20px',
            backgroundColor: '#212232',
            borderRadius: '20px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            margin: '25px',
            paddingBottom: '50px',
        }}>
            <div style={{
                marginTop: '15px',
                display: 'flex',
                justifyContent: 'space-between',
                width: '100%',
                maxWidth: '100%',
            }}>
                <input
                    type="search"
                    placeholder="zoek club..."
                    value={filterText}
                    onChange={(e) => setFilterText(e.target.value)}
                    className='focus:outline-none'
                    style={{
                        width: '350px',
                        padding: '10px 18px',
                        background: '#2a2a3d',
                        border: '1px solid #444',
                        fontSize: '16px',
                        boxSizing: 'border-box',
                        marginRight: '10px',
                        transition: 'border-color 0.3s',
                        borderRadius: '5px',
                    }}
                />
                <Button
                    name='Toevoegen'
                    onclick={handleAdd}
                    style={{ width: '150px' }}
                />
            </div>

            <table style={{
                width: '100%',
                borderCollapse: 'collapse',
                borderRadius: '10px',
                boxShadow: '0 2px 10px rgba(0, 0, 0, 0.05)',
                marginTop: '20px',
            }}>
                <thead>
                    <tr style={{
                        color: 'white',
                    }}>
                        <th style={{
                            fontSize: '22px',
                            fontWeight: 'bold',
                            padding: '15px',
                            borderBottom: '2px solid white',
                            textAlign: 'center',
                        }} colSpan={2}>
                            Club
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {filteredOrganisations.map((org: Organisations) => (
                        <tr key={org.id} style={{
                            borderBottom: '1px solid white',
                            transition: 'background-color 0.3s',
                        }}
                            onMouseEnter={(e) => { e.currentTarget.style.backgroundColor = '#1e202d'; }}
                            onMouseLeave={(e) => { e.currentTarget.style.backgroundColor = 'transparent'; }}
                        >
                            <td style={{
                                fontSize: '20px',
                                color: 'white',
                                padding: '15px 20px',
                            }}>{org.name}</td>
                            <td style={{
                                padding: '15px 20px',
                                textAlign: 'right',
                            }}>
                                <button onClick={() => handleEdit(org.id)} title="Edit" style={{
                                    marginRight: '20px',
                                    backgroundColor: 'transparent',
                                    border: 'none',
                                    cursor: 'pointer',
                                }}>
                                    <FontAwesomeIcon icon={faEdit} style={{ fontSize: '20px', color: 'lightblue' }} />
                                </button>
                                <button onClick={() => handleDelete(org.id)} title="Delete" style={{
                                    backgroundColor: 'transparent',
                                    border: 'none',
                                    cursor: 'pointer',
                                }}>
                                    <FontAwesomeIcon icon={faTrash} style={{ fontSize: '22px', color: 'red' }} />
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <style>
                {`
                    input[type="search"]::-webkit-search-cancel-button {
                        -webkit-filter: invert(50%); /* Invert the color to make it white if background is dark */
                    }
                `}
            </style>
        </div>
    );
};

export default OrganisationList;
