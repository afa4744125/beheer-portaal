import React, { useEffect, useState } from 'react';
import validator from 'validator';
import { useParams, useNavigate } from 'react-router-dom';
import InputField from '../../shared/components/form/inputfield';
import Button from '../../shared/components/form/button';
import { useGetOrganisationApiQuery, useUpdateOrganisationApiMutation } from '../../redux/services/organisation';
import Succes from '../../shared/components/form/succes';
import Error from '../../shared/components/form/error';

const OrganisationEdit = () => {
    const { id } = useParams<{ id: string }>();
    const [organisationName, setOrganisationName] = useState('');
    const [organisationEmail, setOrganisationEmail] = useState('');

    const [error, setError] = useState('');
    const [succes, setSucces] = useState('');

    const { data: organisation } = useGetOrganisationApiQuery(id!);
    const [updateOrganisation, { isLoading }] = useUpdateOrganisationApiMutation();

    const navigate = useNavigate();

    useEffect(() => {
        if (organisation) {
            setOrganisationName(organisation.name);
            setOrganisationEmail(organisation.email);
        }
    }, [organisation]);

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            if (!validator.isEmail(organisationEmail)) {
                setError('Ongeldig e-mailadres');
                return;
            }
            const updatedOrganisation = {
                id: id!,
                name: organisationName,
                email: organisationEmail,
            };
            await updateOrganisation(updatedOrganisation).unwrap();
            navigate('/admin/club');
        } catch (err) {
            console.error('Failed to update organisation: ', err);
        }
    };

    const SuccesReset = () => {
        setSucces('');
    };

    const ErrorReset = () => {
        setError('');
    };

    return (
        <div
            style={{
                padding: '20px',
                backgroundColor: '#212232',
                borderRadius: '20px',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                margin: '25px',
                paddingBottom: '50px',
            }}
        >
            <h1 style={{ fontSize: '34px', marginBottom: '40px' }}>Organisatie Updaten</h1>
            <Error message={error} onClose={ErrorReset} style={{ width: '100%' }} />
            <Succes message={succes} onClose={SuccesReset} style={{ width: '100%' }} />
            <form onSubmit={handleSubmit} style={{ width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <InputField
                    label='Organisation Name'
                    placeholder='Naam'
                    value={organisationName}
                    onChange={(e) => setOrganisationName(e.target.value)}
                    style={{ width: '600px', marginBottom: '30px' }}
                    required
                />
                <InputField
                    label='Organisation Email'
                    placeholder='Email'
                    value={organisationEmail}
                    onChange={(e) => setOrganisationEmail(e.target.value)}
                    style={{ width: '600px' }}
                    required
                />


                <Button
                    name='Opslaan'
                    isLoading={isLoading}
                    style={{ width: '150px' }}
                />
            </form>
        </div>
    );
};

export default OrganisationEdit;
