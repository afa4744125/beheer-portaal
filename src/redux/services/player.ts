import { createApi } from '@reduxjs/toolkit/query/react';
import type { Player, CreatePlayer, Filters, PlayerListResponse, importRequest } from '../types/player';
import { baseQueryWithAuth } from '../apiConfig';

export const playerApi = createApi({
    reducerPath: 'playerApi',
    baseQuery: baseQueryWithAuth,
    endpoints: (builder) => ({

        getPlayersApi: builder.query<PlayerListResponse, Filters>({
            query: (filter) => ({
                url: `players/`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: filter,
            }),
        }),

        ExportPlayersApi: builder.query<Player[], Filters>({
            query: (filter) => ({
                url: `export/players/`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: filter,
            }),
        }),

        getPlayerApi: builder.query<Player, string>({
            query: (id) => ({
                url: `player/${id}`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        createPlayerApi: builder.mutation<string, CreatePlayer>({
            query: (player) => ({
                url: 'player/',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: player,
            }),
        }),

        importPlayerApi: builder.mutation<string, importRequest>({
            query: (players) => ({
                url: 'player/import/',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: players,
            }),
        }),

        updatePlayerApi: builder.mutation<string, Player>({
            query: (player) => ({
                url: `player/${player.id}`,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: player,
            }),
        }),

        deletePlayerApi: builder.mutation<void, string>({
            query: (id) => ({
                url: `player/${id}`,
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),
    }),
});

export const { useGetPlayerApiQuery, useCreatePlayerApiMutation, useUpdatePlayerApiMutation, useDeletePlayerApiMutation, useGetPlayersApiQuery, useExportPlayersApiQuery, useImportPlayerApiMutation } = playerApi;
