import { createApi } from '@reduxjs/toolkit/query/react';
import type { Data, Datas } from '../types/data';
import { baseQueryWithAuth } from '../apiConfig';

export const dataApi = createApi({
    reducerPath: 'dataApi',
    baseQuery: baseQueryWithAuth,
    endpoints: (builder) => ({

        datasApi: builder.query<Datas[], void>({
            query: () => ({
                url: `datas`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        getDataApi: builder.query<Datas, string>({
            query: (id) => ({
                url: `data/${id}`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        createDataApi: builder.mutation<string, Data>({
            query: (data) => ({
                url: 'data/',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: data,
            }),
        }),

        updateDataApi: builder.mutation<string, Data>({
            query: (data) => ({
                url: `data/${data.id}`,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: data,
            }),
        }),

        deleteDataApi: builder.mutation<void, string>({
            query: (id) => ({
                url: `data/${id}`,
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),
    }),
});

export const {
    useDatasApiQuery,
    useGetDataApiQuery,
    useCreateDataApiMutation,
    useUpdateDataApiMutation,
    useDeleteDataApiMutation
} = dataApi;
