import { createApi } from '@reduxjs/toolkit/query/react';
import type { Organisation, CreateOrganisation, getOrganisation, Organisations, Thema, settings, dashboard } from '../types/organisation';
import { baseQueryWithAuth } from '../apiConfig';

export const organisationApi = createApi({
    reducerPath: 'organisationApi',
    baseQuery: baseQueryWithAuth,
    endpoints: (builder) => ({
        publicListApi: builder.query<Organisations[], void>({
            query: () => ({
                url: 'organisations',
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        getOrganisationApi: builder.query<getOrganisation, string>({
            query: (id) => ({
                url: `organisation/${id}`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        createOrganisationApi: builder.mutation<string, CreateOrganisation>({
            query: (organisation) => ({
                url: 'organisation/',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: organisation,
            }),
        }),

        updateOrganisationApi: builder.mutation<string, Organisation>({
            query: (organisation) => ({
                url: `organisation/${organisation.id}`,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: organisation,
            }),
        }),

        deleteOrganisationApi: builder.mutation<void, string>({
            query: (id) => ({
                url: `organisation/${id}`,
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        setThemaApi: builder.mutation<string, Thema>({
            query: (Thema) => ({
                url: `thema/${Thema.org_id}`,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: Thema,
            }),
        }),

        getThemaApi: builder.query<Thema, string>({
            query: (id) => ({
                url: `thema/${id}`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        setSettingsApi: builder.mutation<string, settings>({
            query: (settings) => ({
                url: `settings/organisation/${settings.org_id}`,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: settings,
            }),
        }),

        getSettingsApi: builder.query<settings, string>({
            query: (org_id) => ({
                url: `settings/organisation/${org_id}`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        getdashboardApi: builder.query<dashboard, string>({
            query: (org_id) => ({
                url: `dashboard/${org_id}`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

    }),
});

export const { usePublicListApiQuery, useGetOrganisationApiQuery, useDeleteOrganisationApiMutation, useCreateOrganisationApiMutation, useUpdateOrganisationApiMutation, useSetThemaApiMutation, useGetThemaApiQuery, useSetSettingsApiMutation, useGetSettingsApiQuery, useGetdashboardApiQuery } = organisationApi;
