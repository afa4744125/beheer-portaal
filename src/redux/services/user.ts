import { createApi } from '@reduxjs/toolkit/query/react';
import type { login, LoginResponse, User, CreateUser } from '../types/user';
import { baseQueryWithAuth } from '../apiConfig';

export const userApi = createApi({
    reducerPath: 'userApi',
    baseQuery: baseQueryWithAuth,
    endpoints: (builder) => ({
        loginApi: builder.mutation<LoginResponse, login>({
            query: (data) => ({
                url: 'login_check',
                method: 'POST',
                body: data,
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        usersApi: builder.query<User[], string>({
            query: (org_id) => ({
                url: `${org_id}/users`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        getUserApi: builder.query<User, string>({
            query: (id) => ({
                url: `user/${id}`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        createUserApi: builder.mutation<string, CreateUser>({
            query: (user) => ({
                url: 'user/',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: user,
            }),
        }),

        updateUserApi: builder.mutation<string, User>({
            query: (user) => ({
                url: `user/${user.id}`,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: user,
            }),
        }),

        deleteUserApi: builder.mutation<void, string>({
            query: (id) => ({
                url: `user/${id}`,
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),
    }),
});

export const { useLoginApiMutation, useUsersApiQuery, useGetUserApiQuery, useCreateUserApiMutation, useUpdateUserApiMutation, useDeleteUserApiMutation } = userApi;
