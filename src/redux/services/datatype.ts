import { createApi } from '@reduxjs/toolkit/query/react';
import type { DataType, DataTypes } from '../types/datatype';
import { baseQueryWithAuth } from '../apiConfig';

export const datatypeApi = createApi({
    reducerPath: 'datatypeApi',
    baseQuery: baseQueryWithAuth,
    endpoints: (builder) => ({


        adminListApi: builder.query<DataTypes[], void>({
            query: () => ({
                url: 'admin/datatypes',
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        dataTypesApi: builder.query<DataTypes[], void>({
            query: () => ({
                url: `datatypes`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        getDataTypeApi: builder.query<DataTypes, string>({
            query: (id) => ({
                url: `datatype/${id}`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        createDataTypeApi: builder.mutation<string, DataType>({
            query: (datatype) => ({
                url: 'datatype/',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: datatype,
            }),
        }),

        updateDataTypeApi: builder.mutation<string, DataType>({
            query: (datatype) => ({
                url: `datatype/${datatype.id}`,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: datatype,
            }),
        }),

        deleteDataTypeApi: builder.mutation<void, string>({
            query: (id) => ({
                url: `datatype/${id}`,
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),
    }),
});

export const {
    useAdminListApiQuery,
    useDataTypesApiQuery,
    useGetDataTypeApiQuery,
    useCreateDataTypeApiMutation,
    useUpdateDataTypeApiMutation,
    useDeleteDataTypeApiMutation,
} = datatypeApi;
