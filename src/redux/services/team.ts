import { createApi } from '@reduxjs/toolkit/query/react';
import type { Team, createTeam } from '../types/team';
import { baseQueryWithAuth } from '../apiConfig';

export const teamApi = createApi({
    reducerPath: 'teamApi',
    baseQuery: baseQueryWithAuth,
    endpoints: (builder) => ({
        teamsApi: builder.query<Team[], string>({
            query: (org_id) => ({
                url: `${org_id}/teams`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        getTeamApi: builder.query<Team, string>({
            query: (id) => ({
                url: `team/${id}`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),

        createTeamApi: builder.mutation<string, createTeam>({
            query: (team) => ({
                url: 'team/',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: team,
            }),
        }),

        updateTeamApi: builder.mutation<string, Team>({
            query: (team) => ({
                url: `team/${team.id}`,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: team,
            }),
        }),

        deleteTeamApi: builder.mutation<void, string>({
            query: (id) => ({
                url: `team/${id}`,
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),
    }),
});

export const { useTeamsApiQuery, useGetTeamApiQuery, useCreateTeamApiMutation, useUpdateTeamApiMutation, useDeleteTeamApiMutation } = teamApi;
