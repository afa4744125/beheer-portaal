export type Gender = "man" | "woman";

export interface Player {
    id: string;
    firstname: string;
    infix?: string | null;
    lastname: string;
    membershipnumber: string;
    relationshipnumber?: string | null;
    dateofbirth: string;
    email?: string | null;
    team_id?: string | null;
    team_name?: string | null;
    gender: Gender
}

export interface CreatePlayer {
    org_id: string;
    firstname: string;
    infix?: string | null;
    lastname: string;
    membershipnumber?: string;
    relationshipnumber?: string | null;
    dateofbirth: string;
    email?: string | null;
    team_id?: string | null;
    gender: Gender
}

export interface importRequest {
    org_id: string;
    player: importPlayer[];
}

export interface importPlayer {
    firstname: string;
    infix?: string | null;
    lastname: string;
    membershipnumber?: string;
    relationshipnumber?: string | null;
    dateofbirth: string;
    email?: string | null;
    team?: string | null;
    gender: Gender
}

export interface Filter {
    membershipnumber?: string;
    firstname?: string | null;
    lastname?: string | null;
    email?: string | null;
    team_id?: string | null;
    gender?: Gender | null;
    dateofbirth?: string | null;
    relationshipnumber?: string | null;
}

export interface Filters {
    org_id: string;
    filter: Filter;
    pagesize?: number;
    page?: number;
}

export interface PlayerListResponse {
    players: Player[];
    totalPlayers: number;
}