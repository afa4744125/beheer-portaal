export interface Data {
    id?: string;
    value: string;
    player: string;
    datatype: string;
}

export interface Datas {
    id: string;
    value: string;
    player: string;
    createdby: string;
    datatype_name: string;
    datatype_id: string;
}
