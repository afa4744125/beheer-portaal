export interface Team {
    id: string;
    name: string;
}

export interface createTeam {
    org_id: string;
    name: string;
}