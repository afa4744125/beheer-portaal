

export type Role = "ROLE_ADMIN" | "ROLE_USER";

export interface login {
    email: string;
    password: string;
}

export interface LoginResponse {
    token: string;
}

export interface User {
    id: string;
    firstname: string;
    lastname: string;
    email: string;
    role: Role[];
    teams?: string[] | null;
}

export interface CreateUser {
    org_id: string;
    firstname: string;
    lastname: string;
    email: string;
    role: Role[];
    password: string;
    teams?: string[] | null;
}