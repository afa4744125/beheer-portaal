
export type Status = "active" | "inactive";

export interface DataType {
    id?: string;
    name: string;
    number?: boolean;
    status?: Status;
}

export interface DataTypes {
    id: string;
    name: string;
    number: boolean;
    status: Status;
}
