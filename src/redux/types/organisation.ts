export interface Organisation {
    id: string;
    name: string;
    email: string;
}

export interface CreateOrganisation {
    name: string;
    email: string;
}

export interface getOrganisation {
    name: string;
    email: string;
}

export interface Organisations {
    id: string;
    name: string;
}

export interface Thema {
    org_id: string;
    primaryColor?: string;
    secondaryColor?: string;
    tertiaryColor?: string;
    logo?: string;
}

export interface settings {
    org_id: string;
    email: string;
}

export interface dashboard {
    totalPlayers: number;
    totalPlayersDelete: number;
    totalTeams: number;
    totalUsers: number;
}
