import { BaseQueryFn, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { RootState } from './store';

const baseQuery = fetchBaseQuery({
    baseUrl: 'http://localhost:8000/api/',
    prepareHeaders: (headers, { getState }) => {
        const token = (getState() as RootState).auth.token;
        if (token) {
            headers.set('Authorization', `Bearer ${token}`);
        }
        return headers;
    },

});

export const baseQueryWithAuth: BaseQueryFn = async (args, api, extraOptions) => {
    return await baseQuery(args, api, extraOptions);
};
