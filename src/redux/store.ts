import { configureStore } from '@reduxjs/toolkit';
import { authReducer } from './authslice';
import { organisationApi } from './services/organisation';
import { teamApi } from './services/team';
import { userApi } from './services/user';
import { playerApi } from './services/player';
import { datatypeApi } from './services/datatype';
import { dataApi } from './services/data';

export type RootState = ReturnType<typeof store.getState>;

const store = configureStore({
    reducer: {
        auth: authReducer,
        [userApi.reducerPath]: userApi.reducer,
        [organisationApi.reducerPath]: organisationApi.reducer,
        [teamApi.reducerPath]: teamApi.reducer,
        [playerApi.reducerPath]: playerApi.reducer,
        [datatypeApi.reducerPath]: datatypeApi.reducer,
        [dataApi.reducerPath]: dataApi.reducer
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware()
            .concat(userApi.middleware)
            .concat(organisationApi.middleware)
            .concat(teamApi.middleware)
            .concat(playerApi.middleware)
            .concat(datatypeApi.middleware)
            .concat(dataApi.middleware),
});

export default store;
