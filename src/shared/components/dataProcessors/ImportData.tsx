import { importPlayer, Gender } from '../../../redux/types/player';
import validator from 'validator';

export const processCSV = (data: string): { players: importPlayer[], errors: string[] } => {
    const rows = data.split('\n').map(row => row.trim()).filter(row => row.length > 0);

    if (rows.length < 2) {
        return { players: [], errors: ['Het bestand bevat geen gegevens of alleen header.'] };
    }

    const headers = rows[0].split(/[,;]+/).map(header => header.replace(/\(.*$/, '').trim().toLowerCase());


    const requiredFields = ['membershipnumber', 'gender', 'firstname', 'lastname', 'dateofbirth'];
    const errors: string[] = [];
    const players: importPlayer[] = [];

    const dateFormatDMY = /^\d{1,2}-\d{1,2}-\d{4}$/;
    const dateFormatYMD = /^\d{4}-\d{1,2}-\d{1,2}$/;

    const isValidDate = (date: string): boolean => {
        const [day, month, year] = date.includes('-') && dateFormatDMY.test(date)
            ? date.split('-').map(Number)
            : [NaN, NaN, NaN];

        const [yearYMD, monthYMD, dayYMD] = date.includes('-') && dateFormatYMD.test(date)
            ? date.split('-').map(Number)
            : [NaN, NaN, NaN];

        const finalYear = year || yearYMD;
        const finalMonth = (month || monthYMD) - 1;
        const finalDay = day || dayYMD;

        const parsedDate = new Date(finalYear, finalMonth, finalDay);

        return (
            !isNaN(parsedDate.getTime()) &&
            parsedDate.getFullYear() === finalYear &&
            parsedDate.getMonth() === finalMonth &&
            parsedDate.getDate() === finalDay
        );
    };

    const translateGender = (gender: string): Gender => {
        const genderMap: { [key: string]: Gender } = {
            "man": "man",
            "male": "man",
            "vrouw": "woman",
            "female": "woman",
            "woman": "woman"
        };

        const lowerGender = gender.toLowerCase();
        return genderMap[lowerGender as Gender];
    };


    rows.slice(1).forEach((row, rowIndex) => {
        let error = 0
        const columns = row.split(/[,;]+/).map(col => col.trim());

        const player: importPlayer = {
            membershipnumber: columns[headers.indexOf('lidnummer')] || '0',
            gender: translateGender(columns[headers.indexOf('geslacht')]) || undefined,
            firstname: columns[headers.indexOf('voornaam')] || '',
            infix: columns[headers.indexOf('tussenvoegsel')] || undefined,
            lastname: columns[headers.indexOf('achternaam')] || '',
            relationshipnumber: columns[headers.indexOf('relatienummer')] || undefined,
            team: columns[headers.indexOf('team')] || undefined,
            dateofbirth: columns[headers.indexOf('geboortedatum')] || '',
            email: columns[headers.indexOf('email')] || undefined
        };

        const missingFields = requiredFields.filter(field => {
            const value = player[field as keyof importPlayer];
            return value === '' || value === undefined || value === null;
        });

        if (player.email && !validator.isEmail(player.email)) {
            errors.push(`Rij ${rowIndex + 1}: Ongeldig e-mailadres.`);
            error++;
        }

        if (
            (!dateFormatDMY.test(player.dateofbirth) &&
                !dateFormatYMD.test(player.dateofbirth)) ||
            !isValidDate(player.dateofbirth)
        ) {
            errors.push(
                `Rij ${rowIndex + 1}: Ongeldige geboortedatum. Verwacht geldig formaat: d-m-j of j-m-d.`
            );
            error++;
        }

        if (missingFields.length > 0) {
            errors.push(
                `Rij ${rowIndex + 1}: Ontbrekende verplichte veldt ${missingFields.join(', ')}.`
            );
            error++;
        }

        if (error === 0) {
            players.push(player);
        }
    });

    return { players, errors };
};





