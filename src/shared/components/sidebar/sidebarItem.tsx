import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useSidebar } from './sidebarContext';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons';

type SidebarItemProps = {
    label: string;
    route?: string;
    children?: React.ReactNode;
    icon?: IconProp;
};

const SidebarItem: React.FC<SidebarItemProps> = ({ label, route, children, icon }) => {
    const navigate = useNavigate();
    const { selectedRoute, setSelectedRoute, lastClickedItem, toggleLastClickedItem } = useSidebar();

    const handleParentClick = () => {
        if (route) {
            navigate(route);
            setSelectedRoute(route);
        } else {
            toggleLastClickedItem(label);
        }
    };

    const handleChildClick = (childRoute: string) => {
        navigate(childRoute);
        setSelectedRoute(childRoute);
    };

    const hasChildren = React.Children.count(children) > 0;

    const isSelected = selectedRoute === route || React.Children.toArray(children).some(child =>
        React.isValidElement(child) && child.props.route === selectedRoute
    );

    const isOpen = isSelected || lastClickedItem === label;

    return (
        <div>
            <div
                onClick={handleParentClick}
                style={{
                    cursor: 'pointer',
                    padding: '12px',
                    paddingLeft: '20px',
                    marginLeft: '90px',
                    fontSize: '16px',
                    fontWeight: 'bold',
                    backgroundColor: isSelected ? '#161625' : 'transparent',
                    color: 'white',
                    borderTopLeftRadius: '50px',
                    borderBottomLeftRadius: '50px',
                    display: 'flex',
                    alignItems: 'center'
                }}
            >
                {icon && <FontAwesomeIcon icon={icon} style={{ fontSize: '20px', paddingRight: '8px' }} />}
                {label}
                {hasChildren && (
                    <FontAwesomeIcon
                        icon={isOpen ? faChevronDown : faChevronUp}
                        style={{ marginLeft: 'auto', transition: 'transform 0.2s' }}
                    />
                )}
            </div>
            {
                isOpen && hasChildren && (
                    <div style={{ paddingLeft: '20px', marginTop: '10px' }}>
                        {React.Children.map(children, (child) => (
                            React.isValidElement(child) && (
                                <div onClick={() => handleChildClick(child.props.route)} style={{ cursor: 'pointer', borderRadius: '4px' }}>
                                    {child}
                                </div>
                            )
                        ))}
                    </div>
                )
            }
        </div>
    );
};

export default SidebarItem;
