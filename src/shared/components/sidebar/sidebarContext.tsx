import React, { createContext, useContext, useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';

type SidebarContextType = {
    selectedRoute: string | null;
    setSelectedRoute: (route: string) => void;
    lastClickedItem: string | null;
    toggleLastClickedItem: (item: string) => void;
};

const SidebarContext = createContext<SidebarContextType | undefined>(undefined);

type SidebarProviderProps = {
    children: React.ReactNode;
};

export const SidebarProvider: React.FC<SidebarProviderProps> = ({ children }) => {
    const location = useLocation();
    const [selectedRoute, setSelectedRoute] = useState<string | null>(null);
    const [lastClickedItem, setLastClickedItem] = useState<string | null>(null);

    useEffect(() => {
        if (location.pathname) {
            setSelectedRoute(location.pathname);
        }
    }, [location.pathname]);

    const toggleLastClickedItem = (item: string) => {
        setLastClickedItem((prevItem) => (prevItem === item ? null : item));
    };

    return (
        <SidebarContext.Provider value={{ selectedRoute, setSelectedRoute, lastClickedItem, toggleLastClickedItem }}>
            {children}
        </SidebarContext.Provider>
    );
};

export const useSidebar = () => {
    const context = useContext(SidebarContext);
    if (context === undefined) {
        throw new Error('useSidebar must be used within a SidebarProvider');
    }
    return context;
};
