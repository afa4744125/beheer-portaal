import React, { useState } from 'react';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import DatagridToolbar from './DatagridToolbar';
import { useGetPlayersApiQuery, useDeletePlayerApiMutation } from '../../../redux/services/player';
import { Player, Filters } from '../../../redux/types/player';
import { useNavigate } from 'react-router-dom';
import Actions from '../form/actions';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faEye, faPen } from '@fortawesome/free-solid-svg-icons';
import type { Filter } from '../../../redux/types/player';

interface DatagridProps {
    filter: Filter
}

const Datagrid: React.FC<DatagridProps> = ({ filter }) => {
    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));
    const [pageSize] = useState<number>(25);
    const [page, setPage] = useState<number>(0);

    const navigate = useNavigate();

    const filters: Filters = {
        org_id: org_id!,
        filter: filter,
        pagesize: pageSize,
        page: page + 1,
    };

    const { data, isLoading } = useGetPlayersApiQuery(filters);
    const [deletePlayer] = useDeletePlayerApiMutation();

    const actions = [
        {
            name: 'Bekijken',
            onClick: (id: string) => alert(`Bekijken speler met ID: ${id}`),
            icon: <FontAwesomeIcon icon={faEye} />,
        },
        {
            name: 'Bewerken',
            onClick: (id: string) => navigate(`/player/edit/${id}`),
            icon: <FontAwesomeIcon icon={faPen} />,
        },
        {
            name: 'Verwijderen',
            onClick: async (id: string) => {
                await deletePlayer(id);
                setTimeout(() => window.location.reload(), 2000);
            },
            icon: <FontAwesomeIcon icon={faTrash} />,
        },
    ];

    const columns: GridColDef[] = [
        { field: 'membershipnumber', headerName: 'Lidnummer', width: 100 },
        { field: 'firstname', headerName: 'Voornaam', flex: 1 },
        { field: 'lastname', headerName: 'Achternaam', flex: 1 },
        {
            field: 'dateofbirth',
            headerName: 'Geboortedatum',
            flex: 1,
            renderCell: (params) => {
                const date = params.row.dateofbirth;
                const validDate = moment(date, ['YYYY-MM-DD', 'DD-MM-YYYY', moment.ISO_8601], true);
                return validDate.isValid() ? validDate.format('DD-MM-YYYY') : 'Onbekend';
            }
        },
        {
            field: 'gender',
            headerName: 'Geslacht',
            width: 100,
            renderCell: (params) => params.row.gender === 'man' ? 'man' : params.row.gender === 'woman' ? 'vrouw' : 'Onbekend',
        },
        { field: 'team', headerName: 'Team', width: 90 },
        {
            field: 'actions',
            headerName: '',
            width: 130,
            renderCell: (params) => (
                <Actions
                    actions={actions.map(action => ({
                        ...action,
                        onClick: () => action.onClick(params.row.id),
                    }))}
                    id={params.row.id}
                    rounded
                />
            )
        }
    ];

    const rows = data?.players?.map((player: Player) => ({
        id: player.id,
        membershipnumber: player.membershipnumber,
        firstname: player.firstname,
        lastname: player.infix ? `${player.infix} ${player.lastname}` : player.lastname,
        dateofbirth: player.dateofbirth,
        gender: player.gender || '',
        team: player.team_name || '',
    })) || [];

    const totalItems = data?.totalPlayers || 0;

    if (isLoading) return <div>Loading players...</div>;

    return (
        <div style={{ margin: '25px', padding: '20px', backgroundColor: '#212232', borderRadius: '30px' }}>
            <DataGrid
                style={{ color: 'white' }}
                rows={rows}
                columns={columns}
                getRowId={(row) => row.id}
                paginationMode="server"
                rowCount={totalItems}
                pageSizeOptions={[pageSize]}
                paginationModel={{ page, pageSize }}
                onPaginationModelChange={(model) => setPage(model.page)}
                checkboxSelection
                autoHeight
                slots={{
                    toolbar: (props) => <DatagridToolbar {...props} filter={filter} />,
                }}
            />
        </div>
    );
};

export default Datagrid;
