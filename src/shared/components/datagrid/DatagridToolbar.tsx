import React, { useState, useEffect } from 'react';
import Actions from '../form/actions';
import Button from '../form/button';
import { useExportPlayersApiQuery } from '../../../redux/services/player';
import { useGetOrganisationApiQuery } from '../../../redux/services/organisation';
import { Player, Filters } from '../../../redux/types/player';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { GridToolbarContainer } from '@mui/x-data-grid';
import { useNavigate } from 'react-router-dom';
import { GridToolbarProps } from '@mui/x-data-grid';
import type { Filter } from '../../../redux/types/player';

interface DatagridToolbarProps extends GridToolbarProps {
    filter: Filter
}

const actions = [
    {
        name: 'Verwijderen',
        onClick: () => {
            alert(`verwijder speler met ID:`);
            setTimeout(() => {
                window.location.reload();
            }, 2000);
        },
        icon: <FontAwesomeIcon icon={faTrash} />,
    },
];

const DatagridToolbar: React.FC<DatagridToolbarProps> = ({ filter }) => {
    const [org_id] = useState<string | null>(sessionStorage.getItem('org_id'));
    const navigate = useNavigate();
    const { data: organisation } = useGetOrganisationApiQuery(org_id!);
    const [isExporting, setIsExporting] = useState(false);
    const [exportTriggered, setExportTriggered] = useState(false);

    const filters: Filters = {
        org_id: org_id!,
        filter: filter,
    };

    const { data: players, refetch } = useExportPlayersApiQuery(filters, { skip: !exportTriggered });

    useEffect(() => {
        if (exportTriggered && players) {
            setIsExporting(true);
            const csvData = generateCSV(players);
            const blob = new Blob([csvData], { type: 'text/csv' });
            const url = URL.createObjectURL(blob);

            const now = new Date();
            const dateString = now.toISOString().slice(0, 10);

            const hours = String(now.getHours()).padStart(2, '0');
            const minutes = String(now.getMinutes()).padStart(2, '0');
            const seconds = String(now.getSeconds()).padStart(2, '0');

            const timeString = `${hours}${minutes}${seconds}`;

            const a = document.createElement('a');
            a.href = url;
            a.download = `${organisation!.name}_players_${dateString}_${timeString}.csv`;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
            URL.revokeObjectURL(url);

            setIsExporting(false);
            setExportTriggered(false);
        }
    }, [exportTriggered, players, organisation]);

    const handleExport = () => {
        setExportTriggered(true);
        refetch();
    };

    const generateCSV = (data: Player[]) => {
        const csvRows: string[] = [];
        const headers = ['Lidnummer', 'Geslacht', 'Voornaam', 'Tussenvoegsel', 'Achternaam', 'Relatienummer', 'Team', 'Geboortedatum'];
        csvRows.push(headers.join(';'));

        for (const player of data) {
            const values = [
                player.membershipnumber,
                player.gender === 'man' ? 'man' : player.gender === 'woman' ? 'vrouw' : 'Onbekend',
                player.firstname,
                player?.infix || null,
                player.lastname,
                player?.relationshipnumber || null,
                player?.team_name || null,
                player.dateofbirth,
            ];
            csvRows.push(values.join(';'));
        }
        return csvRows.join('\n');
    };

    return (
        <GridToolbarContainer style={{ display: 'flex', justifyContent: 'space-between', padding: '8px 25px', marginTop: '10px' }}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
                <Actions actions={actions} right />
            </div>
            <div style={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap' }}>
                <Button
                    name='exporteren'
                    style={{ width: '150px', marginRight: '15px', padding: '8px' }}
                    onclick={handleExport}
                    isLoading={isExporting}
                />
                <Button name='importeren' style={{ width: '150px', marginRight: '15px', padding: '8px' }} onclick={() => navigate('/player/import')} />
                <Button name='Toevoegen' style={{ width: '150px', padding: '8px' }} onclick={() => navigate('/player/add')} />
            </div>
        </GridToolbarContainer>
    );
};

export default DatagridToolbar;
