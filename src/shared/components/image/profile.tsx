import React, { useState, useEffect, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleUser } from '@fortawesome/free-regular-svg-icons';
import { faUser, faGear, faRightFromBracket } from '@fortawesome/free-solid-svg-icons';

const Profile: React.FC = () => {
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);
    const [isHovered, setIsHovered] = useState(false);
    const dropdownRef = useRef<HTMLDivElement>(null);

    const handleToggleDropdown = () => {
        setIsDropdownOpen((prev) => !prev);
    };

    const handleSettings = () => {
        console.log('Navigating to settings...');
    };

    const handleLogout = () => {
        sessionStorage.clear();
        window.location.href = '/';
    };

    useEffect(() => {
        const handleClickOutside = (event: MouseEvent) => {
            if (dropdownRef.current && !dropdownRef.current.contains(event.target as Node)) {
                setIsDropdownOpen(false);
            }
        };

        if (isDropdownOpen) {
            document.addEventListener('mousedown', handleClickOutside);
        } else {
            document.removeEventListener('mousedown', handleClickOutside);
        }

        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [isDropdownOpen]);

    return (
        <div style={{ position: 'relative', display: 'inline-block', margin: '25px', }} ref={dropdownRef}>
            <button
                onClick={handleToggleDropdown}
                onMouseEnter={() => setIsHovered(true)}
                onMouseLeave={() => setIsHovered(false)}
                style={{ outline: 'none', background: 'none', border: 'none', cursor: 'pointer' }}
                aria-haspopup="true"
                aria-expanded={isDropdownOpen}
            >
                <FontAwesomeIcon
                    icon={faCircleUser}
                    style={{ fontSize: '40px', color: isHovered ? '#ebebeb' : 'white' }}
                />
            </button>

            {isDropdownOpen && (
                <>
                    <div
                        style={{
                            position: 'absolute',
                            right: '13px',
                            width: '0',
                            height: '0',
                            borderLeft: '6px solid transparent',
                            borderRight: '6px solid transparent',
                            borderBottom: '9px solid white',
                            zIndex: 1000
                        }}
                    ></div>
                    <div style={{ position: 'absolute', right: '0', marginTop: '8px', width: '200px', zIndex: 1000, border: 'solid 1px gray', borderRadius: '8px' }}>
                        <button
                            onMouseEnter={(e) => e.currentTarget.style.backgroundColor = 's#1e1f2b'}
                            onMouseLeave={(e) => e.currentTarget.style.backgroundColor = '#212232'}
                            onClick={handleSettings}
                            style={{
                                display: 'block',
                                width: '100%',
                                textAlign: 'left',
                                padding: '8px',
                                backgroundColor: '#212232',
                                color: 'white',
                                border: 'none',
                                cursor: 'pointer',
                                borderTopLeftRadius: '8px',
                                borderTopRightRadius: '8px',
                            }}
                        >
                            <FontAwesomeIcon
                                icon={faUser}
                                style={{ paddingRight: '5px' }}
                            />
                            Profile
                        </button>
                        <button
                            onMouseEnter={(e) => e.currentTarget.style.backgroundColor = '#1e1f2b'}
                            onMouseLeave={(e) => e.currentTarget.style.backgroundColor = '#212232'}
                            onClick={handleSettings}
                            style={{
                                display: 'block',
                                width: '100%',
                                textAlign: 'left',
                                padding: '8px',
                                backgroundColor: '#212232',
                                color: 'white',
                                border: 'none',
                                borderTop: '1px solid rgba(255, 255, 255, 0.1)',
                                cursor: 'pointer'
                            }}
                        >
                            <FontAwesomeIcon
                                icon={faGear}
                                style={{ paddingRight: '5px' }}
                            />
                            Settings
                        </button>
                        <button
                            onMouseEnter={(e) => e.currentTarget.style.backgroundColor = '#d40404'}
                            onMouseLeave={(e) => e.currentTarget.style.backgroundColor = 'red'}
                            onClick={handleLogout}
                            style={{
                                display: 'block',
                                width: '100%',
                                padding: '8px',
                                backgroundColor: 'red',
                                color: 'white',
                                borderTop: '1px solid rgba(255, 255, 255, 0.1)',
                                borderBottomLeftRadius: '8px',
                                borderBottomRightRadius: '8px',
                                fontWeight: 'bold',
                                cursor: 'pointer'
                            }}
                        >
                            <FontAwesomeIcon
                                icon={faRightFromBracket}
                                style={{ paddingRight: '5px' }}
                            />
                            Logout
                        </button>
                    </div>
                </>
            )}
        </div>
    );
};

export default Profile;
