import React, { useEffect, useState } from 'react';

interface ImageRotateAnimationProps {
    images: string[];
}

const ImageRotateAnimation = ({ images }: ImageRotateAnimationProps) => {
    const num = images.length;
    const [rotation, setRotation] = useState(0);
    const [size, setSize] = useState(400);
    const [radius, setRadius] = useState(300);
    // const [gap, setGap] = useState(100);

    useEffect(() => {
        const updateSizes = () => {
            const containerWidth = window.innerWidth * 0.8;
            const newGap = Math.max(50, containerWidth * 0.02);
            const availableWidth = containerWidth - num * newGap;
            const newSize = Math.min(availableWidth / num * 1.9, 400);
            const newRadius = Math.floor(newSize / (2 * Math.tan(Math.PI / num))) + newGap;

            setSize(newSize);
            // setGap(newGap);
            setRadius(newRadius);
        };

        updateSizes();
        window.addEventListener('resize', updateSizes);
        return () => window.removeEventListener('resize', updateSizes);
    }, [num]);

    useEffect(() => {
        const interval = setInterval(() => {
            setRotation((prevRotation) => prevRotation + 0.3);
        }, 30);
        return () => clearInterval(interval);
    }, []);

    return (
        <div
            style={{
                width: '100%',
                height: '100%',
                perspective: '1200px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                position: 'relative',
            }}
        >
            <div
                id="spinner"
                style={{
                    transform: `rotateY(${rotation}deg)`,
                    transformStyle: 'preserve-3d',
                    width: '100%',
                    height: '100%',
                    position: 'relative',
                    transition: 'transform 0.03s linear',
                }}
            >
                {images.map((img, i) => {
                    const angle = (i * 360) / num;
                    return (
                        <img
                            key={i}
                            src={img}
                            alt={`Image ${i + 1}`}
                            style={{
                                position: 'absolute',
                                transform: `rotateY(${angle}deg) translateZ(${radius}px)`,
                                width: `${size}px`,
                                height: 'auto',
                                top: '50%',
                                left: '50%',
                                transformOrigin: 'center',
                                marginTop: `-${size / 3}px`,
                                marginLeft: `-${size / 1.8}px`,
                            }}
                        />
                    );
                })}
            </div>
        </div>
    );
};

export default ImageRotateAnimation;