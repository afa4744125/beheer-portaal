import React from 'react';
import AfaLogo from '../../../assets/AfaLogobg.png';

interface ShirtProps {
    primaryColor: string;
    secondaryColor: string;
    tertiaryColor: string;
    logo?: string;
    style?: React.CSSProperties;
}

const Outfit = ({ primaryColor, secondaryColor, tertiaryColor, logo, style }: ShirtProps) => {
    return (
        <>
            <div className="football-kit" style={{ ...style }}>
                <img onError={(e) => { e.currentTarget.onerror = null; e.currentTarget.src = AfaLogo; }} src={logo} className="logo" style={{ marginTop: '8px', width: '18px', height: 'auto', position: 'absolute', top: '15%', left: '33%', transform: 'translate(-50%, -50%)', zIndex: 2 }} />
                <div className="football-kit__shirt" style={{ backgroundImage: `repeating-linear-gradient(90deg, ${primaryColor} 0%, ${primaryColor} 33.33%, ${secondaryColor} 33.33%, ${secondaryColor} 66.66%, ${primaryColor} 66.66%, ${primaryColor} 100%)` }} />
                <div className="football-kit__sleeve football-kit__sleeve--left" style={{ background: primaryColor }} />
                <div className="football-kit__sleeve football-kit__sleeve--right" style={{ background: primaryColor }} />
                <div className="football-kit__waist" style={{ background: tertiaryColor }} />
                <div className="football-kit__shorts" style={{ background: tertiaryColor }} />
            </div>
        </>
    );
};

export default Outfit;
