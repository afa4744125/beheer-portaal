import React from 'react';
import AfaLogo from '../../../assets/AfaLogobg.png';

interface BannerProps {
    primaryColor: string;
    secondaryColor: string;
    logo: string;
    style?: React.CSSProperties;
}

const Banner = ({ primaryColor, secondaryColor, logo, style }: BannerProps) => {
    return (
        <>
            <div className="banner" style={{ ...style }}>
                <div className="square" style={{ zIndex: 2, backgroundImage: `repeating-linear-gradient(90deg, ${primaryColor} 0%, ${primaryColor} 33.33%, ${secondaryColor} 34.33%, ${secondaryColor} 66.66%, ${primaryColor} 66.66%, ${primaryColor} 100%)`, backgroundColor: `${secondaryColor}` }} >
                    <img onError={(e) => { e.currentTarget.onerror = null; e.currentTarget.src = AfaLogo }} src={logo} className="logo" style={{ marginTop: '8px', width: '60px', height: 'auto', position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }} />
                </div>
                <div className="triangle-container" style={{ zIndex: 1, height: '25px', width: '100px' }}>
                    <div className="triangle-middle" style={{ backgroundImage: `repeating-linear-gradient(90deg, ${primaryColor} 0%, ${primaryColor} 33.33%, ${secondaryColor} 34.33%, ${secondaryColor} 66.66%, ${primaryColor} 66.66%, ${primaryColor} 100%)` }}></div>
                </div>
            </div >
        </>
    );
};

export default Banner;
