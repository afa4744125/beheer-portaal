import React, { useState, useEffect, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons';

interface Option {
    id: string;
    name: string;
}

interface DropdownProps {
    options: Option[];
    value?: string;
    label?: string;
    placeholder?: string;
    noDefaultSearch?: boolean;
    onChange?: (value: string) => void;
    style?: React.CSSProperties;
}

const Dropdown: React.FC<DropdownProps> = ({ options, value, onChange, style, label, placeholder, noDefaultSearch }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [selectedOptionId, setSelectedOptionId] = useState<string | null>(value || null);
    const [searchTerm, setSearchTerm] = useState('');
    const [filteredOptions, setFilteredOptions] = useState<Option[]>(options);
    const dropdownRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        setSelectedOptionId(value || null);
        setSearchTerm('');
    }, [value, options]);

    useEffect(() => {
        setFilteredOptions(
            options.filter(option =>
                option.name.toLowerCase().includes(searchTerm.toLowerCase())
            )
        );
    }, [searchTerm, options]);

    useEffect(() => {
        const handleClickOutside = (event: MouseEvent) => {
            if (dropdownRef.current && !dropdownRef.current.contains(event.target as Node)) {
                setIsOpen(false);
            }
        };

        document.addEventListener('mousedown', handleClickOutside);
        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [dropdownRef]);

    const toggleDropdown = () => {
        setIsOpen(prev => !prev);
        if (!isOpen && !noDefaultSearch) {
            setSearchTerm(selectedOptionId ? options.find(option => option.id === selectedOptionId)?.name || '' : '');
        } else {
            setSearchTerm('')
        }
    };

    const handleOptionClick = (option: Option) => {
        if (selectedOptionId !== option.id) {
            setSelectedOptionId(option.id);
            setSearchTerm(option.name);
            if (onChange) onChange(option.id);
        }
        setIsOpen(false);
    };

    const handleInputKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter' && searchTerm) {
            const matchedOption = options.find(option => option.name.toLowerCase() === searchTerm.toLowerCase());
            if (matchedOption) handleOptionClick(matchedOption);
        }
    };

    return (
        <div ref={dropdownRef} style={{ position: 'relative', display: 'inline-block', width: '250px', margin: '3px', ...style }}>
            <div>
                {isOpen ? (
                    <div>
                        {label && (
                            <label style={{
                                marginBottom: '6px',
                                color: '#ffffff',
                                fontWeight: '600',
                                fontSize: '14px'
                            }}>
                                {label}
                            </label>
                        )}
                        <input
                            type="search"
                            placeholder="Search..."
                            value={searchTerm}
                            onChange={(e) => setSearchTerm(e.target.value)}
                            onKeyDown={handleInputKeyDown}
                            style={{
                                width: '100%',
                                padding: '10px 12px',
                                border: '1px solid #444',
                                borderRadius: '5px',
                                fontSize: '16px',
                                outline: 'none',
                                backgroundColor: '#2a2a3d',
                                color: '#ffffff',
                                transition: 'border-color 0.3s ease, background-color 0.3s ease, box-shadow 0.3s ease',
                                boxShadow: '0 2px 5px rgba(0, 0, 0, 0.5)',
                            }}
                            onFocus={(e) => {
                                e.currentTarget.style.borderColor = '#007bff';
                                e.currentTarget.style.backgroundColor = '#1e202d';
                                e.currentTarget.style.boxShadow = '0 0 5px rgba(0, 123, 255, 0.5)';
                            }}
                            onBlur={(e) => {
                                e.currentTarget.style.borderColor = '#444';
                                e.currentTarget.style.backgroundColor = '#2a2a3d';
                                e.currentTarget.style.boxShadow = '0 2px 5px rgba(0, 0, 0, 0.5)';
                            }}
                        />
                    </div>
                ) : (
                        <div>
                            {label && (
                                <label style={{
                                    marginBottom: '6px',
                                    color: '#ffffff',
                                    fontWeight: '600',
                                    fontSize: '14px'
                                }}>
                                    {label}
                                </label>
                            )}
                            <button
                                onClick={toggleDropdown}
                                style={{
                                    backgroundColor: '#2a2a3d',
                                    color: value ? '#ffffff' : '#a2abba',
                                    padding: '10px 12px',
                                    border: '1px solid #444',
                                    cursor: 'pointer',
                                    fontSize: '16px',
                                    borderRadius: '5px',
                                    width: '99%',
                                    textAlign: 'left',
                                    outline: 'none',
                                    transition: 'border-color 0.3s ease, background-color 0.3s ease, box-shadow 0.3s ease',
                                    display: 'flex',
                                    alignItems: 'center',
                                    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.5)',
                                }}
                            >
                                {selectedOptionId
                                    ? options.find(option => option.id === selectedOptionId)?.name
                                    : (placeholder ?? 'Selecteer een optie')}
                                <FontAwesomeIcon
                                    icon={isOpen ? faChevronUp : faChevronDown}
                                    style={{ marginLeft: 'auto', transition: 'transform 0.2s' }}
                                />
                            </button>
                        </div>
                )}
            </div>
            {isOpen && (
                <div
                    style={{
                        display: 'block',
                        position: 'absolute',
                        backgroundColor: '#212232',
                        border: 'solid 1px gray',
                        minWidth: '100%',
                        boxShadow: '0 4px 8px rgba(0, 0, 0, 0.3)',
                        zIndex: 1000,
                        borderRadius: '8px',
                        maxHeight: '195px',
                        overflowY: 'auto',
                        marginTop: '5px',
                    }}
                >
                    {filteredOptions.length > 0 ? (
                        filteredOptions.map((option) => (
                            <div
                                key={option.id}
                                onClick={() => handleOptionClick(option)}
                                style={{
                                    color: '#ffffff',
                                    padding: '12px 15px',
                                    textDecoration: 'none',
                                    display: 'block',
                                    transition: 'background-color 0.3s',
                                    cursor: 'pointer',
                                    borderBottom: '1px solid rgba(255, 255, 255, 0.1)',
                                    backgroundColor: option.id === selectedOptionId ? '#3a3a5c' : 'transparent',
                                }}
                                onMouseEnter={(e) => (e.currentTarget.style.backgroundColor = '#4a4a6c')}
                                onMouseLeave={(e) => {
                                    e.currentTarget.style.backgroundColor = option.id === selectedOptionId ? '#3a3a5c' : 'transparent';
                                }}
                                role="option"
                                aria-selected={option.id === selectedOptionId}
                                tabIndex={0}
                                onKeyDown={(e) => {
                                    if (e.key === 'Enter') handleOptionClick(option);
                                }}
                            >
                                {option.name}
                            </div>
                        ))
                    ) : (
                        <div style={{ color: '#ffffff', padding: '12px 15px', textAlign: 'center' }}>
                                geen optie gevonden
                        </div>
                    )}
                </div>
            )}
        </div>
    );
};

export default Dropdown;
