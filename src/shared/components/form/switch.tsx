import { motion } from "framer-motion";

type ToggleProps = {
    checked: boolean;
    onChange: (checked: boolean) => void;
    title: string;
    style?: React.CSSProperties;
};

export default function Switch({ checked, onChange, title, style }: ToggleProps) {
    return (
        <div style={{ display: "flex", flexDirection: 'column', alignItems: 'center', ...style }}>
            <span style={{ marginRight: "10px", color: "#fff", fontWeight: "bold", marginBottom: '10px' }}>
                {title}
            </span>
            <button
                onClick={() => onChange(!checked)}
                style={{
                    background: checked ? "#007bff" : "#2a2a3d",
                    border: "1px solid #444",
                    padding: "10px 12px",
                    width: "60px",
                    height: "32px",
                    display: "flex",
                    alignItems: "center",
                    cursor: "pointer",
                    transition: "border-color 0.3s ease, background-color 0.3s ease, box-shadow 0.3s ease",
                    boxShadow: "0 2px 5px rgba(0, 0, 0, 0.5)",
                    position: "relative",
                }}
                className="rounded-full"
            >
                <motion.div
                    style={{
                        width: "24px",
                        height: "24px",
                        background: "white",
                        borderRadius: "50%",
                        position: "absolute",
                        left: checked ? "30px" : "4px",
                        transition: "left 0.3s ease",
                        boxShadow: "0 2px 5px rgba(0, 0, 0, 0.5)",
                    }}
                />
            </button>
        </div>
    );
}
