import { motion } from "framer-motion";
import React, { useState, useEffect } from "react";

type SliderProps = {
    value: number;
    onChange: (value: number) => void;
    min: number;
    max: number;
    title: string;
    style?: React.CSSProperties;
};

export default function Slider({ value, onChange, min, max, title, style }: SliderProps) {
    const percentage = ((value - min) / (max - min)) * 100;
    const [isEditing, setIsEditing] = useState(false);
    const [inputValue, setInputValue] = useState(value);

    const handleKeyDown = (e: React.KeyboardEvent) => {
        if (e.key === "ArrowLeft" || e.key === "ArrowDown") {
            onChange(Math.max(min, value - 1));
        } else if (e.key === "ArrowRight" || e.key === "ArrowUp") {
            onChange(Math.min(max, value + 1));
        } else if (e.key === "Enter") {
            handleBlur();
        }
    };

    const handleDoubleClick = () => {
        setIsEditing(true);
    };

    const handleBlur = () => {
        setIsEditing(false);
        setInputValue(value);
    };

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = Number(e.target.value);
        if (!isNaN(newValue)) {
            setInputValue(newValue);
            onChange(Math.max(min, Math.min(max, newValue)));
        }
    };

    useEffect(() => {
        setInputValue(value);
    }, [value]);

    return (
        <div style={{ display: "flex", flexDirection: "row", alignItems: "center", ...style }}>
            <span style={{ color: "#fff", fontWeight: "bold", flex: '1', marginTop: '20px' }}>{title}</span>
            <div style={{ display: "flex", flexDirection: "column", alignItems: "center", flex: '3' }}>
                <div style={{ display: "flex", justifyContent: "space-between", width: "200px" }}>
                    <span style={{ color: "#fff" }}>{min === value ? '' : min}</span>
                    <span style={{ color: "#fff" }}>{max === value ? '' : max}</span>
                </div>
                <div
                    style={{
                        width: "200px",
                        height: "16px",
                        background: "#2a2a3d",
                        position: "relative",
                        cursor: "pointer",
                    }}
                    className="rounded-full"
                    onClick={(e) => {
                        const rect = (e.currentTarget as HTMLElement).getBoundingClientRect();
                        let newValue = min + ((e.clientX - rect.left) / rect.width) * (max - min);
                        newValue = Math.max(min, Math.min(max, Math.round(newValue)));
                        onChange(newValue);
                    }}
                    tabIndex={0}
                    onKeyDown={handleKeyDown}
                    onDoubleClick={handleDoubleClick}
                >
                    <motion.div
                        style={{
                            width: `${percentage}%`,
                            height: "100%",
                            background: "#007bff",
                        }}
                        className="rounded-full"
                    />
                    <motion.div
                        style={{
                            width: "16px",
                            height: "16px",
                            background: "white",
                            borderRadius: "50%",
                            position: "absolute",
                            left: `calc(${Math.max(percentage, 7)}% - 15px)`,
                            transform: "translateY(-50%)",
                            top: "50%",
                            boxShadow: "0 2px 5px rgba(0, 0, 0, 0.5)",
                        }}
                        dragConstraints={{ left: 0, right: 200, top: 0, bottom: 0 }}
                        dragElastic={0}
                        dragMomentum={false}
                    />
                    {isEditing ? (
                        <input
                            type="number"
                            value={inputValue}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            autoFocus
                            style={{
                                position: "absolute",
                                left: `calc(${Math.max(percentage, 7)}% - 35px)`,
                                top: "-35px",
                                transform: "translateX(-50%)",
                                color: "#fff",
                                fontWeight: "bold",
                                fontSize: "18px",
                                width: "60px",
                                background: "#333",
                                border: "1px solid #fff",
                                borderRadius: "4px",
                                textAlign: "center",
                            }}
                        />
                    ) : (
                        <motion.div
                            style={{
                                position: "absolute",
                                left: `calc(${Math.max(percentage, 7)}% - 7px)`,
                                top: "-35px",
                                transform: "translateX(-50%)",
                                color: "#fff",
                                fontWeight: "bold",
                                fontSize: "18px",
                            }}
                        >
                            {value}
                        </motion.div>
                    )}
                </div>
            </div>
        </div>
    );
}
