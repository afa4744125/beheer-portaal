import React from 'react';

interface ColorPickerProps {
    value?: string;
    onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    label?: string;
    style?: React.CSSProperties;
}

const ColorPicker: React.FC<ColorPickerProps> = ({ value = '#ffffff', onChange, label, style }) => {
    const handleHexChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        const isValidHex = /^#([0-9A-F]{3}){1,2}$/i.test(newValue);
        if (isValidHex || newValue === '') {
            onChange?.(e);
        }
    };

    return (
        <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '16px', maxWidth: '100%', ...style }}>
            {label && (
                <label style={{
                    marginBottom: '6px',
                    color: '#ffffff',
                    fontWeight: '600',
                    fontSize: '14px'
                }}>
                    {label}
                </label>
            )}
            <div style={{
                display: 'flex',
                alignItems: 'center',
                position: 'relative',
                border: '1px solid #444',
                padding: '10px',
                borderRadius: '8px',
                backgroundColor: '#1f1f2e',
                boxShadow: '0 4px 12px rgba(0, 0, 0, 0.2)',
                transition: 'all 0.3s ease',
            }}>
                <div style={{
                    width: '36px',
                    height: '36px',
                    borderRadius: '50%',
                    background: value,
                    border: '2px solid #444',
                    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.3)',
                    marginRight: '10px'
                }} />
                <input
                    style={{
                        appearance: 'none',
                        border: 'none',
                        background: 'transparent',
                        color: '#fff',
                        fontSize: '16px',
                        flex: '1',
                        padding: '6px',
                        borderRadius: '4px',
                        cursor: 'pointer',
                        outline: 'none',
                        transition: 'border-color 0.3s ease, box-shadow 0.3s ease',
                        textAlign: 'center',
                        fontFamily: 'inherit',
                    }}
                    type="color"
                    value={value}
                    onChange={onChange}
                />
                <input
                    style={{
                        marginLeft: '10px',
                        backgroundColor: '#2a2a3d',
                        border: '1px solid #444',
                        borderRadius: '5px',
                        padding: '6px 12px',
                        fontSize: '14px',
                        color: '#ffffff',
                        outline: 'none',
                        width: '100px',
                        textAlign: 'center',
                        boxShadow: '0 2px 5px rgba(0, 0, 0, 0.5)',
                    }}
                    type="text"
                    value={value}
                    onChange={handleHexChange}
                    placeholder="#000000"
                />
            </div>
        </div>
    );
}

export default ColorPicker;
