import React, { useState, useRef, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faChevronUp, } from '@fortawesome/free-solid-svg-icons';

interface Action {
  name: string;
  onClick: (id: string) => void;
  backgroundColor?: string;
  icon?: React.ReactNode;
}

interface ActionsProps {
  actions: Action[];
  id?: string;
  rounded?: boolean;
  right?: boolean;
}

const Actions: React.FC<ActionsProps> = ({ actions, id, rounded, right }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedAction] = useState<Action | null>(null);
  const dropdownRef = useRef<HTMLDivElement>(null);
  const buttonref = useRef<HTMLDivElement>(null);

  const toggleDropdown = () => {
    setIsOpen((prev) => !prev);
  };

  const handleActionClick = (action: Action) => {
    if (action.onClick) {
      action.onClick(id ?? '');
    }
    setIsOpen(false);
  };

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (
        dropdownRef.current &&
        buttonref.current &&
        !dropdownRef.current.contains(event.target as Node) &&
        !buttonref.current.contains(event.target as Node)
      ) {
        setIsOpen(false);
      }
    };

    const handleScroll = () => {
      setIsOpen(false);
    };

    if (isOpen) {
      document.addEventListener('mousedown', handleClickOutside);
      window.addEventListener('scroll', handleScroll, true);
    } else {
      document.removeEventListener('mousedown', handleClickOutside);
      window.addEventListener('scroll', handleScroll, true);
    }

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
      window.addEventListener('scroll', handleScroll, true);
    };

  }, [isOpen]);

  return (
    <div
      ref={buttonref}
      style={{
        zIndex: '20',
        padding: '6px 0',
        height: '100%',
        overflow: 'hidden',
      }}
    >
      <button
        onClick={toggleDropdown}
        style={{
          backgroundColor: '#2a2a3d',
          color: selectedAction ? '#ffffff' : '#a2abba',
          padding: rounded ? '5px 15px' : '9px 20px',
          maxHeight: '100%',
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          border: '1px solid #444',
          cursor: 'pointer',
          fontSize: '16px',
          borderRadius: rounded ? '20px' : '0',
          textAlign: 'left',
          outline: 'none',
          transition: 'background-color 0.3s, border-color 0.3s',
          display: 'flex',
          alignItems: 'center',
          boxShadow: '0 4px 8px rgba(0, 0, 0, 0.2)',
          marginRight: '30px',
        }}
      >
        {selectedAction ? selectedAction.name : 'Acties'}
        <FontAwesomeIcon
          icon={isOpen ? faChevronDown : faChevronUp}
          style={{ transition: 'transform 0.2s', marginLeft: '15px' }}
        />
      </button>
      {isOpen &&
        ReactDOM.createPortal(
          <div
            ref={dropdownRef}
            style={{
              position: 'absolute',
              zIndex: 1500,
              backgroundColor: '#212232',
              border: '1px solid gray',
              minWidth: '200px',
              maxWidth: '200px',
              boxShadow: '0 4px 8px rgba(0, 0, 0, 0.3)',
              borderRadius: '8px',
              top: buttonref.current?.getBoundingClientRect().bottom || 0,
              left: (buttonref.current?.getBoundingClientRect().left || 0) - (right ? 0 : 92),
            }}
          >
            {actions.map((action, index) => (
              <div
                key={index}
                onClick={() => handleActionClick(action)}
                style={{
                  color: '#ffffff',
                  padding: '8px 10px',
                  cursor: 'pointer',
                  transition: 'background-color 0.3s',
                  borderBottom: '1px solid rgba(255, 255, 255, 0.1)',
                  backgroundColor: action.backgroundColor || 'transparent',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'flex-start',
                }}
                onMouseEnter={(e) => (e.currentTarget.style.backgroundColor = '#4a4a6c')}
                onMouseLeave={(e) => (e.currentTarget.style.backgroundColor = action.backgroundColor || 'transparent')}
              >
                {action.icon && (
                  <span style={{ marginRight: '8px', textAlign: 'left' }}>
                    {action.icon}
                  </span>
                )}
                <span style={{
                  flexGrow: 1,
                  textAlign: 'center',
                }}>{action.name}</span>
              </div>
            ))}
          </div>,
          document.body
        )
      }
    </div >
  );
};

export default Actions;
