import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp } from "@fortawesome/fontawesome-svg-core";

interface CardProps {
    title?: string;
    number: string;
    color: string;
    icon?: IconProp;
    style?: React.CSSProperties;
    onClick?: () => void;
}

const Card: React.FC<CardProps> = ({ title, number, color, icon, style, onClick }) => {
    const [count, setCount] = useState(0);
    const targetNumber = parseInt(number, 10);

    useEffect(() => {
        const duration = 1000;
        const stepTime = 40;
        const steps = duration / stepTime;
        const increment = targetNumber / steps;

        let start = 0;
        const timer = setInterval(() => {
            start += increment;
            if (start >= targetNumber) {
                setCount(targetNumber);
                clearInterval(timer);
            } else {
                setCount(Math.round(start));
            }
        }, stepTime);

        return () => clearInterval(timer);
    }, [targetNumber]);

    return (
        <>
            <div
                style={{
                    position: "relative",
                    overflow: "hidden",
                    color: color,
                    backgroundColor: color,
                    borderRadius: "30px",
                    ...style,
                    padding: "3px",
                    animation: "cardAnimation 1s linear",
                }}
            >

                <div
                    style={{
                        padding: "25px 40px",
                        backgroundColor: "#212232",
                        borderRadius: "30px",
                        width: "100%",
                        height: "100%",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        boxShadow: "0 6px 15px rgba(0, 0, 0, 0.3)",
                        cursor: "pointer",
                    }}
                    onClick={onClick}
                >
                    <div
                        style={{
                            width: "100%",
                            display: "flex",
                            justifyContent: "space-between",
                            marginBottom: "15px",
                        }}
                    >
                        <div style={{ color: "white", fontSize: "18px", fontWeight: "bold", marginRight: "25px" }}>
                            {title}
                        </div>
                        {icon && <FontAwesomeIcon icon={icon} style={{ color, fontSize: "35px" }} />}
                    </div>

                    <div style={{ flexGrow: 1 }}></div>

                    <div
                        style={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            width: "100%",
                        }}
                    >
                        <div
                        style={{
                            color: color,
                                fontSize: "55px",
                                fontWeight: "700",
                                lineHeight: "1",
                                transition: "opacity 0.5s ease-in-out",
                                position: "relative",
                        }}
                        >
                            {count}
                        </div>
                    </div>
                </div>
            </div>
        </>

    );

};

export default Card;