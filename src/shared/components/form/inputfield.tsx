import React from 'react';

interface InputFieldProps {
    value?: string | null;
    onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    placeholder?: string;
    label?: string;
    style?: React.CSSProperties;
    autofocus?: boolean;
    required?: boolean;
}

const InputField: React.FC<InputFieldProps> = ({ value, onChange, placeholder, label, style, autofocus = false, required = false }) => {
    return (
        <div style={{ display: 'flex', flexDirection: 'column', maxWidth: '100%', ...style }}>
            {label && (
                <label style={{
                    marginBottom: '6px',
                    color: '#ffffff',
                    fontWeight: '600',
                    fontSize: '14px'
                }}>
                    {label}{required && <span style={{ color: 'red', marginLeft: '4px' }}>*</span>}
                </label>
            )}
            <input
                style={{
                    background: '#2a2a3d',
                    border: '1px solid #444',
                    borderRadius: '5px',
                    padding: '10px 12px',
                    fontSize: '16px',
                    color: '#ffffff',
                    outline: 'none',
                    transition: 'border-color 0.3s ease, background-color 0.3s ease, box-shadow 0.3s ease',
                    width: '100%',
                    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.5)',
                    cursor: 'text',
                }}
                autoFocus={autofocus}
                className='focus:outline-none'
                type="text"
                value={value ?? undefined}
                onChange={onChange}
                placeholder={placeholder}
                required={required}
                onFocus={(e) => {
                    e.currentTarget.style.borderColor = '#007bff';
                    e.currentTarget.style.backgroundColor = '#1e202d';
                    e.currentTarget.style.boxShadow = '0 0 5px rgba(0, 123, 255, 0.5)';
                }}
                onBlur={(e) => {
                    e.currentTarget.style.borderColor = '#444';
                    e.currentTarget.style.backgroundColor = '#2a2a3d';
                    e.currentTarget.style.boxShadow = '0 2px 5px rgba(0, 0, 0, 0.5)';
                }}
            />
        </div>
    );
}

export default InputField;
