import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

interface PasswordInputfieldProps {
    value?: string
    onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    placeholder?: string;
    label?: string;
    style?: React.CSSProperties;
    required?: boolean;
}

const PasswordInputfield: React.FC<PasswordInputfieldProps> = ({ value, onChange, placeholder, label, style, required = false }) => {

    const [showPassword, setShowPassword] = useState(false);

    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };

    return (
        <div style={{ position: 'relative', ...style }}>
            <label style={{
                marginBottom: '6px',
                color: '#ffffff',
                fontWeight: '600',
                fontSize: '14px'
            }}>
                {label}{required && <span style={{ color: 'red', marginLeft: '4px' }}>*</span>}
            </label>
            <input
                type={showPassword ? 'text' : 'password'}
                placeholder={placeholder}
                className='p-2 px-4 focus:outline-none'
                style={{
                    background: '#2a2a3d',
                    border: '1px solid #444',
                    borderRadius: '5px',
                    padding: '10px 12px',
                    width: '100%',
                    fontSize: '16px',
                    color: '#ffffff',
                    outline: 'none',
                    transition: 'border-color 0.3s ease, background-color 0.3s ease, box-shadow 0.3s ease',
                    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.5)',
                }}
                required={required}
                onChange={onChange}
                value={value}
                onFocus={(e) => {
                    e.currentTarget.style.borderColor = '#007bff';
                    e.currentTarget.style.backgroundColor = '#1e202d';
                    e.currentTarget.style.boxShadow = '0 0 5px rgba(0, 123, 255, 0.5)';
                }}
                onBlur={(e) => {
                    e.currentTarget.style.borderColor = '#444';
                    e.currentTarget.style.backgroundColor = '#2a2a3d';
                    e.currentTarget.style.boxShadow = '0 2px 5px rgba(0, 0, 0, 0.5)';
                }}
            />
            <span
                onClick={togglePasswordVisibility}
                style={{ position: 'absolute', right: '10px', top: '70%', transform: 'translateY(-50%)', cursor: 'pointer', color: 'white' }}
            >
                <FontAwesomeIcon icon={showPassword ? faEyeSlash : faEye} />
            </span>
        </div>
    );
}

export default PasswordInputfield;
