import React from 'react';

interface FilePickerProps {
    onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    label?: string;
    name?: string;
    style?: React.CSSProperties;
}

const FilePicker: React.FC<FilePickerProps> = ({ onChange, label, style, name }) => {
    return (
        <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '16px', maxWidth: '100%', ...style }}>
            {label && (
                <label style={{
                    marginBottom: '6px',
                    color: '#ffffff',
                    fontWeight: '600',
                    fontSize: '14px'
                }}>
                    {label}
                </label>
            )}
            <div style={{
                position: 'relative',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#2a2a3d',
                border: '1px solid #444',
                borderRadius: '8px',
                padding: '12px 20px',
                cursor: 'pointer',
                boxShadow: '0 4px 12px rgba(0, 0, 0, 0.2)',
                transition: 'background-color 0.3s ease, box-shadow 0.3s ease',
                overflow: 'hidden',
                textAlign: 'center',
                width: '100%',
            }}
                onMouseEnter={(e) => e.currentTarget.style.backgroundColor = '#3f3f54'}
                onMouseLeave={(e) => e.currentTarget.style.backgroundColor = '#2a2a3d'}
            >
                <label
                    htmlFor="file-upload"
                    style={{
                        color: '#fff',
                        fontSize: '16px',
                        fontWeight: '500',
                        display: 'block',
                        textAlign: 'center',
                        cursor: 'pointer',
                        zIndex: 1,
                    }}
                >
                    {name}
                </label>
                <input
                    id="file-upload"
                    type="file"
                    accept="image/*"
                    onChange={onChange}
                    style={{
                        position: 'absolute',
                        opacity: 0,
                        cursor: 'pointer',
                        zIndex: 2,
                        height: '100%',
                        width: '100%',
                    }}
                />
            </div>
        </div>
    );
};

export default FilePicker;