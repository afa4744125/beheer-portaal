import React, { useRef, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';

interface InputFieldProps {
    value?: Date | string;
    onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    label?: string;
    style?: React.CSSProperties;
    required?: boolean;
}

const DatePicker: React.FC<InputFieldProps> = ({ value, onChange, label, style, required = false }) => {
    const inputRef = useRef<HTMLInputElement | null>(null);

    const formattedValue = value instanceof Date ? value.toISOString().split('T')[0] : value;

    useEffect(() => {
        if (inputRef.current) {
            inputRef.current.addEventListener('click', () => {
                inputRef.current?.showPicker();
            });
        }
    }, []);

    return (
        <div style={{ position: 'relative', display: 'flex', flexDirection: 'column', marginBottom: '16px', maxWidth: '100%', ...style }}>
            {label && (
                <label style={{
                    marginBottom: '6px',
                    color: '#ffffff',
                    fontWeight: '600',
                    fontSize: '14px'
                }}>
                    {label}{required && <span style={{ color: 'red', marginLeft: '4px' }}>*</span>}
                </label>
            )}
            <input
                ref={inputRef}
                style={{
                    background: '#2a2a3d',
                    border: '1px solid #444',
                    borderRadius: '5px',
                    padding: '10px 12px',
                    paddingRight: '40px',
                    fontSize: '16px',
                    color: value ? '#ffffff' : '#a2abba',
                    outline: 'none',
                    appearance: 'none',
                    transition: 'border-color 0.3s ease, background-color 0.3s ease, box-shadow 0.3s ease',
                    width: '100%',
                    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.5)'
                }}
                type="date"
                value={formattedValue}
                onChange={onChange}
                required={required}
                onFocus={(e) => {
                    e.currentTarget.style.borderColor = '#007bff';
                    e.currentTarget.style.backgroundColor = '#1e202d';
                    e.currentTarget.style.boxShadow = '0 0 5px rgba(0, 123, 255, 0.5)';
                }}
                onBlur={(e) => {
                    e.currentTarget.style.borderColor = '#444';
                    e.currentTarget.style.backgroundColor = '#2a2a3d';
                    e.currentTarget.style.boxShadow = '0 2px 5px rgba(0, 0, 0, 0.5)';
                }}
            />
            <FontAwesomeIcon
                icon={faCalendar}
                style={{
                    position: 'absolute',
                    right: '12px',
                    top: '70%',
                    transform: 'translateY(-50%)',
                    color: '#ffffff',
                    fontSize: '18px',
                    marginRight: '2px'
                }}
            />
        </div>
    );
}

export default DatePicker;
