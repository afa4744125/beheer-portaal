import React from 'react';

interface ButtonProps {
    name: string;
    isLoading?: boolean;
    onclick?: () => void;
    style?: React.CSSProperties;
}

const Button: React.FC<ButtonProps> = ({ name, onclick, style, isLoading = false }) => {

    return (

        <button
            type="submit"
            className="border border-white text-white hover:border-gray-300 hover:text-gray-300 transition duration-200"
            style={{
                padding: '12px',
                borderRadius: '50px',
                background: 'transparent',
                fontSize: '18px',
                boxSizing: 'border-box',
                marginLeft: '10px',
                cursor: 'pointer',
                ...style
            }}
            disabled={isLoading}
            onClick={onclick}
        >
            {isLoading ? `${name}...` : `${name}`}
        </button>
    );
}

export default Button;
