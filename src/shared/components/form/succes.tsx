import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

interface ErrorDisplayProps {
    title?: string;
    message: string;
    onClose?: () => void;
    style?: React.CSSProperties;
}

const Succes: React.FC<ErrorDisplayProps> = ({ title, message, onClose, style }) => {
    if (!message) {
        return null;
    }

    return (
        <div style={{ width: 'auto', margin: '30px', backgroundColor: '#4CAF50', position: 'relative', border: '1px solid white', ...style }} className="my-7 px-4 py-5 text-center">
            {onClose && (
                <button
                    onClick={onClose}
                    style={{
                        position: 'absolute',
                        top: '1px',
                        right: '4px',
                        background: 'none',
                        border: 'none',
                        color: 'white',
                        fontSize: '15px',
                        cursor: 'pointer',
                        marginBottom: '20px',
                    }}
                >
                    <FontAwesomeIcon icon={faTimes} />
                </button>
            )}
            {title && (
                <h2 className="font-bold text-xl mb-2">{title}</h2>
            )}
            <a style={{ color: 'white', fontSize: '20px' }}>{message}</a>
        </div>
    );
};

export default Succes;
